-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 27 Feb 2019 pada 08.40
-- Versi server: 10.1.34-MariaDB
-- Versi PHP: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_webmif`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `capaian_matkul`
--

CREATE TABLE `capaian_matkul` (
  `id_capaian_matkul` int(3) NOT NULL,
  `data_file` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `capaian_matkul`
--

INSERT INTO `capaian_matkul` (`id_capaian_matkul`, `data_file`) VALUES
(5, 'FORM_DATA_CALON_KARYAWAN.pdf');

-- --------------------------------------------------------

--
-- Struktur dari tabel `matrik_matkul`
--

CREATE TABLE `matrik_matkul` (
  `id_matrik_matkul` int(3) NOT NULL,
  `data_file` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `matrik_matkul`
--

INSERT INTO `matrik_matkul` (`id_matrik_matkul`, `data_file`) VALUES
(4, '07_Buku_WEKA_Lintang.pdf');

-- --------------------------------------------------------

--
-- Struktur dari tabel `profile_matkul`
--

CREATE TABLE `profile_matkul` (
  `id_profile_matkul` int(3) NOT NULL,
  `data_file` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `profile_matkul`
--

INSERT INTO `profile_matkul` (`id_profile_matkul`, `data_file`) VALUES
(4, 'TL-WN821N_6_0.pdf');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sebaran_matkul`
--

CREATE TABLE `sebaran_matkul` (
  `id_sebaran_matkul` int(3) NOT NULL,
  `data_file` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `sebaran_matkul`
--

INSERT INTO `sebaran_matkul` (`id_sebaran_matkul`, `data_file`) VALUES
(4, '11.pdf');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tm_ajar`
--

CREATE TABLE `tm_ajar` (
  `id` int(11) NOT NULL,
  `tm_matkul_id` varchar(10) NOT NULL,
  `tm_dosen_id` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tm_bahan_ajar`
--

CREATE TABLE `tm_bahan_ajar` (
  `id` int(3) NOT NULL,
  `tm_matkul_id` varchar(10) NOT NULL,
  `rps_idn` varchar(100) NOT NULL,
  `last_rps_ind` varchar(200) DEFAULT NULL,
  `rps_eng` varchar(100) NOT NULL,
  `last_rps_eng` varchar(200) DEFAULT NULL,
  `bkpm_ind` varchar(100) NOT NULL,
  `last_bkpm_ind` varchar(200) DEFAULT NULL,
  `bkpm_eng` varchar(100) NOT NULL,
  `last_bkpm_eng` varchar(200) DEFAULT NULL,
  `silabus_idn` varchar(100) NOT NULL,
  `last_silabu_ind` varchar(200) DEFAULT NULL,
  `silabus_eng` varchar(100) NOT NULL,
  `last_silabu_eng` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tm_bahan_ajar`
--

INSERT INTO `tm_bahan_ajar` (`id`, `tm_matkul_id`, `rps_idn`, `last_rps_ind`, `rps_eng`, `last_rps_eng`, `bkpm_ind`, `last_bkpm_ind`, `bkpm_eng`, `last_bkpm_eng`, `silabus_idn`, `last_silabu_ind`, `silabus_eng`, `last_silabu_eng`) VALUES
(9, 'MIF1602', '07_Buku_WEKA_Lintang.pdf', NULL, '3276-8277-1-PB.pdf', NULL, 'Wahyu-Pebrianto.pdf', NULL, '14.pdf', NULL, 'FORM_DATA_CALON_KARYAWAN.pdf', NULL, 'CV.pdf', NULL),
(10, 'MIF2603', '07_Buku_WEKA_Lintang1.pdf', NULL, 'FORM_DATA_CALON_KARYAWAN_(4).pdf', NULL, '07_Buku_WEKA_Lintang2.pdf', NULL, 'equal-length.pdf', NULL, '11_(1).xlsx', NULL, 'WhatsApp Image 2019-02-19 at 02_48_32.jpeg', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tm_dosen`
--

CREATE TABLE `tm_dosen` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `nip` varchar(25) DEFAULT NULL,
  `nidn` varchar(20) DEFAULT NULL,
  `jabatan` varchar(50) DEFAULT NULL,
  `prodi` int(11) DEFAULT NULL,
  `tmp_lahir` varchar(50) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `alamat` text NOT NULL,
  `telp` text NOT NULL,
  `email` text NOT NULL,
  `pns` int(11) NOT NULL,
  `gol` varchar(50) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `id_sinta` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tm_dosen`
--

INSERT INTO `tm_dosen` (`id`, `nama`, `nip`, `nidn`, `jabatan`, `prodi`, `tmp_lahir`, `tgl_lahir`, `alamat`, `telp`, `email`, `pns`, `gol`, `image`, `id_sinta`) VALUES
(1, 'Adi Heru Utomo, S.Kom, M.Kom', '19711115 199802 1 001', '0015117106', 'Lektor Kepala', 1, '-', '1971-11-15', '-', '0', '-', 1, 'Pembina/IVa', 'img/ahu.png', 0),
(2, 'Wahyu Kurnia Dewanto, S.Kom, MT', '19710408 200112 1 003', '0008047103', 'Lektor', 1, 'Banyuwangi', '1971-04-08', 'Perumahan Kaliurang Cluster B1 Jember', '081 232 545 875', 'wahyu.k.dewanto@gmail.com', 1, 'Penata/IIIc', 'img/wkd.png', 0),
(3, 'Ika Widiastuti, S.ST, MT', '19780819 200502 2 001', '0019087803', 'Lektor', 1, 'Banyumas', '1978-08-19', 'Perum BTN Mastrip BB-1D', '081249794912', 'ika_widiastuti@polije.ac.id', 1, 'Penata/IIIc', 'img/iw.png', 0),
(4, 'Didit Rahmat Hartadi, S.Kom, MT', '19770929 200501 1 003', '0029097704', 'Lektor', 1, 'Jember', '1977-09-29', 'Panjilaras Indah no. 69 Arjasa Jember', '085234609168', 'diditrhartadi@polije.ac.id', 1, 'Penata/IIIc', 'img/drh.png', 0),
(5, 'Syamsul Arifin, S.Kom, M.Cs', '19810615 200604 1 002', '0015068202', 'Lektor', 1, 'Jember', '1981-06-15', 'Perum Pesona Regency No AE 12 Patrang Jember', '081249515151', 'sy4v1.arifin@gmail.com', 1, 'Penata/IIIc', 'img/sa.png', 0),
(6, 'Hendra Yufit Riskiawan, S.Kom, M.Cs', '19830203 200604 1 003', '0003028302', 'Lektor', 1, 'Jember', '1983-02-03', 'Griya Permata Kampus B7', '085649222290', 'hendra.yufit@gmail.com', 1, 'Penata/IIIc', 'img/hyr.png', 0),
(7, 'Dwi Putro Sarwo Setyohadi, S.Kom, M.Kom', '19800517 200812 1 002', '0017058003', 'Lektor', 1, 'Semarang', '1980-05-17', 'Perum PTP Kaliurang V3 Jember', '085641500007', 'dwi.putro.sarwo.setyohadi@gmail.com', 1, 'Penata/IIIc', 'img/dps.png', 0),
(8, 'Nanik Anita Mukhlisoh, S.ST, MT', '19860609 200812 2 004', '0009068601', 'Asisten Ahli', 1, '-', '1986-06-09', '-', '0', '-', 1, 'Penata Muda TK.I/IIIb', 'img/nam.png', 0),
(9, 'Taufiq Rizaldi, S.ST., MT', '19890329 201503 1 001', '8815110016', '-', 1, 'Malang', '1989-03-29', 'Graha Permata Indah A-22', '', 'taufiq_r@polije.ac.id', 2, '-', 'img/tr.png', 0),
(10, 'Husin, S.Kom, M.MT', '19880702 201610 1 001', '-', '-', 1, '-', '1988-07-02', '-', '0', '-', 2, '-', 'img/husin.png', 0),
(11, 'Surateno, S.Kom., M.Kom', '19790703 200312 1 001', '0003077902', 'Lektor Kepala', 2, 'Jember', '1979-07-03', 'Jl. Hayam Wuruk XIX A-160A Jember', '085236752542', 'ratno@polije.ac.id', 1, 'Pembina/IVa', 'img/stn.png', 0),
(12, 'Agus Hariyanto, ST, M.Kom ', '19780817 200312 1 005', '0017087804', 'Lektor Kepala', 2, '-', '1978-08-17', '-', '0', '-', 1, 'Pembina/IVa', 'img/ah.png', 0),
(13, 'Hariyono Rakhmad, S.Pd, M.Kom', '19701128 200312 1 001', '0028117002', 'Lektor', 2, '-', '1970-11-28', '-', '0', '-', 1, 'Penata Tk.I/IIId', 'img/hr.png', 0),
(14, 'Denny Trias Utomo, S.Si, MT', '19711009 200312 1 001', '0009107104', 'Lektor', 2, '-', '1971-10-09', '-', '0', '-', 1, 'Penata Tk.I/IIId', 'img/dt.png', 0),
(15, 'Nurul Zainal Fanani, S.ST, MT', '19810115 200501 1 011', '0015018103', 'Lektor', 2, '-', '1981-01-15', '-', '0', '-', 1, 'Penata/IIIc', 'img/nzf.png', 0),
(16, 'Denny Wijanarko, ST, MT', '19780908 200501 1 001', '0008097803', 'Lektor', 2, 'Jember', '1978-09-08', 'Perum. Taman Gading AM 17 Jember', '082334417777', 'dennywijanarko@gmail.com', 1, 'Penata/IIIc', 'img/dw.png', 0),
(17, 'Beni Widiawan, S.ST, MT', '19780816 200501 1 002', '0016087806', 'Lektor', 2, '-', '1978-08-16', '-', '0', '-', 1, 'Penata/IIIc', 'img/bw.png', 0),
(18, 'Yogiswara, ST, MT', '19700929 200312 1 001', '0029097005', 'Lektor', 2, 'Malang', '1970-09-29', 'Perum taman gading EE 19 ', '081249735955', 'yogipoltek@gmail.com', 1, 'Penata/IIIc', 'img/ygw.png', 0),
(19, 'Aji Seto Arifianto, S.ST., M.T.', '19851128 200812 1 002', '0028118502', 'Asisten Ahli', 3, 'Jember', '1985-11-28', 'Jl. Belimbing No.13 Jember Lor, Patrang, Jember', '081232534534', 'ajiseto@gmail.com', 1, 'Penata Muda Tk.I /IIIb', 'img/asa1.png', 0),
(20, 'Bekti Maryuni Susanto, S.Pd.T, M.Kom', '19840625 201504 1 004', '0025068404', '-', 2, 'Gunungkidul', '1984-06-25', 'Perumahan Bumi Tegal Besar EF10 Jember', '082236909384', 'bekti@polije.ac.id', 1, '- / IIIb', 'img/bms.png', 0),
(21, 'Fendik Eko Purnomo, S.Pd, MT', '19860319 201403 1 001', '8899010016', '-', 2, 'Jember', '1986-03-19', 'Jl Rasamala. Perum BTN Baratan Permai 2 No E8. Baratan Patrang Jember', '085730672884-081330949192', 'fendik_eko@polije.ac.id', 2, '-', 'img/fep.png', 0),
(22, 'Ery Setiawan Jullev A, S.Kom, M.Cs', '19890710 201509 1 001', '8891500016', '-', 2, 'Pasuruan', '1989-07-10', 'Jl.sriwijaya I no 13 ', '085648807492', 'setiyawanjullev@gmail.com', 2, '-', 'img/esja.png', 0),
(23, 'Trismayanti Dwi Puspitasari, S.Kom, M.Cs', '19900227 201503 2 001', '8868110016', '-', 2, 'Bandung', '1990-02-27', 'Argopuro 35 patemon tanggul jember', '085859184555', 'trismayantidwipuspitasari@gmail.com', 2, '-', 'img/tdp.png', 0),
(24, 'Nugroho Setyo Wibowo, ST. MT', '19740519 200312 1 002', '0019057403', 'Lektor Kepala', 3, 'Bondowoso', '1974-05-19', 'Perum. Pesona Regency AC-13 Patrang Jember', '085236329999', 'nugroho@polije.ac.id', 1, 'Pembina/IVa', 'img/nsw.png', 0),
(25, 'Prawidya Destarianto, S.Kom, M.T', '19801212 200501 1 001', '0012128001', 'Lektor', 3, 'Probolinggo', '1980-12-12', 'Jl Kaliurang Griya Permata Kampus C9 Jember', '085236090999', 'prawidyadestarianto@gmail.com', 1, 'Penata Tk.I/IIId', 'img/pwd.png', 0),
(26, 'Moch. Munih Dian W, S.Kom, M.T', '19700831 199803 1 001', '0031087001', 'Lektor', 3, '-', '1970-08-31', '-', '0', '-', 1, 'Penata/IIIc', 'img/mmd.png', 0),
(27, 'I Putu Dody Lesmana, ST, MT', '19790921 200501 1 001', '0021097903', 'Lektor', 3, '-', '1979-09-21', '-', '0', '-', 1, 'Penata/IIIc', 'img/ipdl.png', 0),
(28, 'Elly Antika, ST, M.Kom', '19781011 200501 2 002', '0011107802', 'Lektor', 3, 'Jember', '1978-10-11', 'Perum Mastrip EE -10 Jember', '08124946073', 'ellyantika.niam@gmail.com', 1, 'Penata/IIIc', 'img/ea.png', 0),
(29, 'Ratih Ayuninghemi, S.ST, M.Kom', '19860802 201504 2 002', '0702088601', '-', 3, 'Magetan', '1986-08-02', 'Jalan Halmahera III No.1 Sumbersari Jember ', '085651152881', 'ratih_hemi@polije.ac.id', 1, '- / IIIb', 'img/rah.png', 0),
(30, 'Khafidurrohman Agustianto, S.Pd, M.Eng', '19911211 201610 1 001', '-', '-', 3, 'Trenggalek ', '1991-12-11', 'Perum. Grisimai CG. 16, Ponorogo, Jawa Timur ', '085646418027', 'khafid@polije.ac.id', 2, '-', 'img/kha.png', 0),
(31, 'Bety Etikasari, S.Pd, M.Pd', '19920528 201610 2 001', '-', '-', 3, 'Trenggalek', '1992-05-28', 'Gang Sumur Bor No.31, Kaliurang, Jember', '085233975628', 'bety.etikasari@gmail.com', 2, '-', 'img/bes.png', 0),
(37, 'Hermawan Arief Putranto, ST, MT', '19830109 201703 1 001', '-', '-', 1, 'Malang', '1983-01-09', 'Jl Kaliurang Cluster Blok  A no 12', '081252465655', 'mistercloud@gmail.com', 2, '-', 'img/35.png', 0),
(32, 'Arvita Agus Kurniasari, S.ST', '-', '-', '-', 4, 'Surabaya', '0000-00-00', 'Dusun Klanceng RT 1/1 Ajung Jember', '085749340130', 'arvita.agus88@gmail.com', 0, '-', 'img/32.png', 0),
(33, 'Moh. Murti Hanafi, S.Kom', '-', '-', '-', 4, 'Jember', '0000-00-00', 'Jember', '083847600676', 'murti.hanafi@gmail.com', 0, '-', 'img/33.png', 0),
(34, 'Yudistira, S.Kom', '-', '-', '-', 4, 'Malang', '0000-00-00', 'Jember', '085223568965', 'bes@polije.ac.id', 0, '-', 'img/34.png', 0),
(36, 'Ely Mulyadi, SE', '-', '-', '-', 4, 'Brebes', '0000-00-00', 'Perum Demang Mulia A16 Jember', '08124949002', 'elimulyadi@gmail.com', 0, '', 'img/36.png', 0),
(38, 'Syamsiar Kautsar, S.ST, MT', '19910315 201703 1 001', '-', '-', 2, '-', '1991-03-15', '-', '-', '-', 2, '-', 'img/syamsiar.png', 0),
(39, 'Evie Yulianing Hasan, S.Kom', '-', '-', '-', 4, '-', '0000-00-00', '-', '-', '-', 0, '-', 'img/evi.png', 0),
(40, 'Agus Purwadi, ST.MT', '19730831 200801 1 003', NULL, NULL, 2, '', '1973-08-31', 'Dusun Krajan Timur RT.03 RW.09', '', '', 1, '', 'img/ap.png', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tm_grup`
--

CREATE TABLE `tm_grup` (
  `id` int(11) NOT NULL,
  `grup` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tm_grup`
--

INSERT INTO `tm_grup` (`id`, `grup`) VALUES
(1, 'root'),
(2, 'admin'),
(3, 'dosen');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tm_matkul`
--

CREATE TABLE `tm_matkul` (
  `id` varchar(10) NOT NULL,
  `nama_matkul` varchar(100) NOT NULL,
  `tm_rumpun_id` int(2) NOT NULL,
  `semester` int(11) NOT NULL,
  `sks` int(2) NOT NULL,
  `teori` varchar(2) NOT NULL,
  `praktek` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tm_matkul`
--

INSERT INTO `tm_matkul` (`id`, `nama_matkul`, `tm_rumpun_id`, `semester`, `sks`, `teori`, `praktek`) VALUES
('MIF1601', 'Pendidikan,Agama', 1, 1, 2, '2', '0'),
('MIF1602', 'Bahasa,Indonesia', 1, 1, 2, '2', '0'),
('MIF1603', 'Basic,English', 1, 1, 2, '1', '1'),
('MIF1604', 'Dasar,Manajemen', 1, 1, 2, '2', '0'),
('MIF1605', 'Pengantar,Teknologi,Informasi', 1, 1, 2, '2', '0'),
('MIF1606', 'Matematika,Diskrit', 1, 1, 2, '2', '0'),
('MIF1607', 'Algoritma,dan,Pemrograman', 1, 1, 4, '2', '2'),
('MIF1608', 'Pengantar,Basis,Data,dan,Aljabar,Relasional', 1, 1, 2, '2', '0'),
('MIF1609', 'Agroinformatika', 1, 1, 2, '2', '0'),
('MIF2601', 'Pancasila', 1, 2, 2, '2', '0'),
('MIF2602', 'Pendidikan,Kewarganegaraan', 1, 2, 2, '2', '0'),
('MIF2603', 'Komputer,Dasar', 1, 2, 2, '0', '2'),
('MIF2604', 'Pemrograman,Berorientasi,Objek', 1, 2, 3, '1', '2'),
('MIF2605', 'Basis,Data,Relasional', 1, 2, 2, '2', '0'),
('MIF2606', 'Analisis,dan,Desain,Sistem,Informasi', 1, 2, 3, '1', '2'),
('MIF2607', 'Struktur,Data', 1, 2, 2, '1', '1'),
('MIF2608', 'Pemrograman,Berbasis,Desktop', 1, 2, 2, '0', '2'),
('MIF2609', 'Jaringan,Komputer', 1, 2, 2, '1', '1'),
('MIF3601', 'Applied,English', 1, 3, 3, '2', '1'),
('MIF3602', 'Kewirausahaan', 1, 3, 2, '1', '1'),
('MIF3603', 'Pemrograman,Web', 1, 3, 2, '0', '2'),
('MIF3604', 'Interaksi,Manusia,dan,Komputer', 1, 3, 2, '1', '1'),
('MIF3605', 'Sistem,Pendukung,Keputusan', 1, 3, 3, '2', '1'),
('MIF3606', 'Sistem,Basis,Data', 1, 3, 2, '0', '2'),
('MIF3607', 'Rekayasa,Perangkat,Lunak', 1, 3, 3, '1', '2'),
('MIF3608', 'Sistem,Operasi', 1, 3, 3, '2', '1'),
('MIF4601', 'Teknik,Penulisan,Ilmiah', 1, 4, 3, '2', '1'),
('MIF4602', 'Sistem,Informasi,Geografis', 1, 4, 3, '2', '1'),
('MIF4603', 'Pemrograman,Web,Framework', 1, 4, 2, '0', '2'),
('MIF4604', 'Aplikasi,Mobile', 1, 4, 2, '0', '2'),
('MIF4605', 'Manajemen,Basis,Data', 1, 4, 3, '2', '1'),
('MIF4606', 'Kecerdasan,Buatan', 1, 4, 3, '2', '1'),
('MIF4607', 'Datawarehouse', 1, 4, 3, '1', '2'),
('MIF5601', 'Praktek,Kerja,Lapang', 1, 5, 4, '0', '4'),
('MIF5602', 'Praktek,Kerja,Lapang', 1, 5, 4, '0', '4'),
('MIF5603', 'Praktek,Kerja,Lapang', 1, 5, 4, '0', '4'),
('MIF6601', 'Multimedia,Interaktif', 1, 6, 2, '0', '2'),
('MIF6602', 'Proyek,Sistem,Informasi', 1, 6, 3, '1', '2'),
('MIF6603', 'Bisnis,Jasa,Informatika', 1, 6, 3, '2', '1'),
('MIF6604', 'Etika,Profesi', 1, 6, 2, '2', '0'),
('MIF6605', 'Pengembangan,Kepribadian', 1, 6, 3, '2', '1'),
('MIF6606', 'Laporan,Akhir', 1, 6, 4, '0', '4');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tm_rumpun`
--

CREATE TABLE `tm_rumpun` (
  `id` int(11) NOT NULL,
  `rumpun` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tm_rumpun`
--

INSERT INTO `tm_rumpun` (`id`, `rumpun`) VALUES
(1, 'Sistem Informasi'),
(2, 'Pemrograman'),
(3, 'Basis Data'),
(4, 'Jaringan Komputer'),
(5, 'Basis Multimedia'),
(6, 'Kecerdasan Buatan'),
(7, 'Aplikasi Mobile'),
(8, 'Informatika Terapan'),
(9, 'Bahasa dan Komunikasi'),
(10, 'Kewirausahaan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tm_users`
--

CREATE TABLE `tm_users` (
  `id` int(3) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  `tm_grup_id` int(2) NOT NULL,
  `tm_dosen_id` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tm_users`
--

INSERT INTO `tm_users` (`id`, `username`, `password`, `tm_grup_id`, `tm_dosen_id`) VALUES
(3, 'wahyu', 'wahyu', 2, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tm_visimisi`
--

CREATE TABLE `tm_visimisi` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `isi` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tm_visimisi`
--

INSERT INTO `tm_visimisi` (`id`, `nama`, `isi`) VALUES
(1, 'visi', 'Menjadi Program Studi yang mampu menunjang visi POLIJE untuk menghasilkan ahli madya bidang informatika yang berdaya saing di Asia pada tahun 2020.'),
(2, 'misi', 'Menyelenggarakan pendidikan vokasi yang berkarakter dalam bidang informatika, yang  menghasilkan lulusan yang berkualitas, kompeten, bermoral, dan berjiwa wirausaha.'),
(3, 'misi', 'Memperkuat kerjasama dengan stakeholder dalam menjaga keterkinian konten kurikulum serta riset yang sesuai dengan kebutuhan masyarakat.'),
(4, 'misi', 'Mensinergikan keahlian sistem informasi pada bidang pertanian serta bidang ilmu lainnya melalui kegiatan pengabdian kepada masyarakat.');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `capaian_matkul`
--
ALTER TABLE `capaian_matkul`
  ADD PRIMARY KEY (`id_capaian_matkul`);

--
-- Indeks untuk tabel `matrik_matkul`
--
ALTER TABLE `matrik_matkul`
  ADD PRIMARY KEY (`id_matrik_matkul`);

--
-- Indeks untuk tabel `profile_matkul`
--
ALTER TABLE `profile_matkul`
  ADD PRIMARY KEY (`id_profile_matkul`);

--
-- Indeks untuk tabel `sebaran_matkul`
--
ALTER TABLE `sebaran_matkul`
  ADD PRIMARY KEY (`id_sebaran_matkul`);

--
-- Indeks untuk tabel `tm_ajar`
--
ALTER TABLE `tm_ajar`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tm_matkul_id` (`tm_matkul_id`),
  ADD KEY `tm_dosen_id` (`tm_dosen_id`);

--
-- Indeks untuk tabel `tm_bahan_ajar`
--
ALTER TABLE `tm_bahan_ajar`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tm_matkul_id` (`tm_matkul_id`);

--
-- Indeks untuk tabel `tm_dosen`
--
ALTER TABLE `tm_dosen`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tm_grup`
--
ALTER TABLE `tm_grup`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tm_matkul`
--
ALTER TABLE `tm_matkul`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tm_rumpun_id` (`tm_rumpun_id`);

--
-- Indeks untuk tabel `tm_rumpun`
--
ALTER TABLE `tm_rumpun`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tm_users`
--
ALTER TABLE `tm_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tm_grup_id` (`tm_grup_id`),
  ADD KEY `tm_dosen_id` (`tm_dosen_id`);

--
-- Indeks untuk tabel `tm_visimisi`
--
ALTER TABLE `tm_visimisi`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `capaian_matkul`
--
ALTER TABLE `capaian_matkul`
  MODIFY `id_capaian_matkul` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `matrik_matkul`
--
ALTER TABLE `matrik_matkul`
  MODIFY `id_matrik_matkul` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `profile_matkul`
--
ALTER TABLE `profile_matkul`
  MODIFY `id_profile_matkul` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `sebaran_matkul`
--
ALTER TABLE `sebaran_matkul`
  MODIFY `id_sebaran_matkul` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `tm_ajar`
--
ALTER TABLE `tm_ajar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tm_bahan_ajar`
--
ALTER TABLE `tm_bahan_ajar`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `tm_dosen`
--
ALTER TABLE `tm_dosen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT untuk tabel `tm_users`
--
ALTER TABLE `tm_users`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `tm_visimisi`
--
ALTER TABLE `tm_visimisi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `tm_bahan_ajar`
--
ALTER TABLE `tm_bahan_ajar`
  ADD CONSTRAINT `tm_bahan_ajar_ibfk_1` FOREIGN KEY (`tm_matkul_id`) REFERENCES `tm_matkul` (`id`);

--
-- Ketidakleluasaan untuk tabel `tm_matkul`
--
ALTER TABLE `tm_matkul`
  ADD CONSTRAINT `tm_matkul_ibfk_1` FOREIGN KEY (`tm_rumpun_id`) REFERENCES `tm_rumpun` (`id`);

--
-- Ketidakleluasaan untuk tabel `tm_users`
--
ALTER TABLE `tm_users`
  ADD CONSTRAINT `tm_users_ibfk_1` FOREIGN KEY (`tm_grup_id`) REFERENCES `tm_grup` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
