<section class="content-header">
    <h1>
        Mata Kuliah
        <small>List</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">User</a></li>
        <li class="active">List</li>
    </ol>
</section>

<section class="content">

    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Daftar User</h3>
    
        </div>

        <div class="box-header">


            <a href="<?php echo base_url(); ?>User/input" >
                <button class="btn btn-success pull-right" type="button">
                    <div>
                        <i class="fa fa-plus-square"></i> TAMBAH
                    </div>
                </button>
            </a>


        </div>

            <div class="box-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Username</th>
                        <th>Grup</th>
                        <th>Nama</th>

                        <th width="7%">Opsi</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach (@$daftar as $value) {?>
                    <tr>
                        <td><input class="checkbox id_checkbox" type="checkbox" name="id[]" value="<?php echo $value->id; ?>" </td>
                        <td><?php echo $value->username; ?></td>
                        <td><?php echo $value->grup; ?></td>
                        <td><?php echo $value->nama; ?></td>

                        <td>
                            <?php
                            echo '<a href="'.base_url().'User/edit/'.$value->id.'" title="Edit User">
                                    <button type="button" class="btn btn-xs btn-success btn-flat"><i class="fa fa-edit"></i></button></a>';
                            echo '<a href="'.base_url().'User/delete/'.$value->id.'" title="Delete User">
                                    <button type="button" class="btn btn-xs btn-warning btn-flat"><i class="fa fa-trash-o"></i></button></a>';
                            ?>
                        </td>
                    </tr>
                    <?php } ?>

                    </tbody>
                </table>
            </div>
        </div>

</section>


<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>