<section class="content-header">
    <h1>
        Data User
        <small>Form Input</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">User</a></li>
        <li class="active">Input</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="box">
        <div class="box-header">
            <i class="fa fa-table"></i>
            <h2 class="box-title">Input User</h2>
            <!-- tools box -->
            <!-- /. tools -->
        </div>
        <?php
        foreach ($user as $value){
            //echo $value->id;


            ?>

        <div class="box-body">
            <form action="<?php echo base_url(). 'User/update'; ?>" method="post">
                <div class="form-group">
                    <div class="col-md-3">
                        <label>Kode User</label>
                        <input type="text" class="form-control" name="id" placeholder="id" readonly value="<?php echo $value->id; ?>"/>
                    </div>
                    <div class="col-md-6">
                        <label>Username</label>
                        <input type="text" class="form-control" name="username" placeholder="Username" value="<?php echo $value->username; ?>"/>
                    </div>
                    <div class="col-md-3">
                        <label>Password</label>
                        <input type="password" class="form-control" name="password" placeholder="Password" value="<?php echo $value->password; ?>"/>
                    </div>
                </div>

                <div class="form-group">
                   <div class="col-md-6">
                        <label>Grup</label>
                         <select class="form-control" name="tm_group_id">
                           <?php foreach ($grup as $row){?> 
                            <option value="<?php echo $row->id ?>"><?php echo $row->grup ?></option><?php } ?>
                         </select>
                    </div>
                    <div class="col-md-6">
                        <label>Dosen</label>
                         <select class="form-control" name="tm_dosen_id">
                           <?php foreach ($dosen as $row){?> 
                            <option value="<?php echo $row->id ?>"><?php echo $row->nama ?></option><?php } ?>
                         </select>
                    </div>
                </div>
                <?php } ?>
                <br>

                <div class="form-group">
                    <div class="col-md-4">
                    <?php echo form_submit('SUBMIT','SIMPAN', array('class'=>'btn btn-primary btn-flat'));?>
                    <a href="<?php echo base_url()?>User" class="btn btn-danger btn-flat" role="button">KEMBALI</a>
                    </div>
                </div>


            </form>
        </div>

        <div class="box-footer clearfix">

        </div>
    </div>

</section>