<?php
$cek_user = $this->session->userdata("session_user");
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>Manajemen Informatika</title>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/wahyu/css/bootstrap.min.css">

    <!-- FontAwesome CSS -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/wahyu/css/font-awesome.min.css">

    <!-- Styles -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/wahyu/style.css">
</head>



<body class="hero">
    <nav class="navbar navbar-expand-lg navbar-dark  fixed-top">
        <!--<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">-->
        <div class="container">
            <a class="navbar-brand"><img style="width: 20%;" src="http://jti.polije.ac.id/mif//assets/pic/logo_mif.png"
                    height="50" width="100"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="<?php echo base_url()?>">Home
                            <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item">
                    <?php
                    if($cek_user!=null){
                        echo '<li class="nav-item"><a class="nav-link" href="'.base_url('Auth/admin').'">Dashboard</a></li>';
                        echo '<li class="nav-item"><a class="nav-link">'.$cek_user.'</a></li>';
                    }else{
                        echo '<li class="nav-item"><a class="nav-link" href="'.base_url('auth').'">Login</a></li>';
                    }
                    ?>
                </li>
                </ul>
            </div>
        </div>
    </nav>



    <div class="outer-container">
        <div class="container portfolio-page">
            <div class="row">
                <div class="col">
                    <br>
                    <ul class="breadcrumbs flex align-items-center">
                        <h1 class="my-4 text-center text-lg-left">Manajemen Informatika</h1>
                    </ul><!-- .breadcrumbs -->
                </div><!-- .col -->
            </div><!-- .row -->

            <!-- Footer -->

        </div><!-- .container -->
    </div><!-- .outer-container -->
    <div style="padding-bottom:100px;" class="sticky-top">
        <div class="container">
            <div style="border-radius: 5px;" class="padding-bottom:100px; card-group">
                <div style="background-color:gray;" class="card ">
                    <div class="card-body text-center">
                        <form action="/action_page.php">
                            <div align="center" class="form-group">
                                <label for="sel1">Pilih Mata Kuliah:</label>
                                <select style=" width:40%;" class="form-control" id="sel1">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                </select>
                            </div>

                            <button type="submit" class="btn btn-primary">Submit</button>

                        </form>
                        <br>
                        <div style="padding-bottom:100px;" class="module">
                            <div class="module-body">
                                <div class="row">
                                    <div class="col-10 col-md-9 col-lg-1">
                                    </div>
                                    <div class="col-10 col-md-9 col-lg-10">
                                        <div class="portfolio-content">
                                            <table style="background-color:rgba(199, 185, 185, 0.644);" class="table table-hover">
                                                <tr>
                                                    <th style="width:100px;"><label for="">bahasa indonesia</label></th>
                                                    <th style="width:100px;"><label for="">bahasa inggris</label></th>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <label for="">Android</label>
                                                        <br>
                                                        <img style="width:100px;" src="<?php echo base_url()?>assets/wahyu/images/logo/pdf.png" alt="Italian Trulli">
                                                        <br>
                                                        <a href=""><button type="button" class="btn btn-primary">buka</button></a>
                                                        <button type="button" class="btn btn-success">download</button>
                                                    </td>
                                                    <td>
                                                        <label for="">Android</label>
                                                        <br>
                                                        <img style="width:100px;" src="<?php echo base_url()?>assets/wahyu/images/logo/pdf.png" alt="Italian Trulli">
                                                        <br>
                                                        <a href=""><button type="button" class="btn btn-primary">buka</button></a>
                                                        <button type="button" class="btn btn-success">download</button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <label for="">Android</label>
                                                        <br>
                                                        <img style="width:100px;" src="<?php echo base_url()?>assets/wahyu/images/logo/pdf.png" alt="Italian Trulli">
                                                        <br>
                                                        <a href=""><button type="button" class="btn btn-primary">buka</button></a>
                                                        <button type="button" class="btn btn-success">download</button>
                                                    </td>
                                                    <td>
                                                        <label for="">Android</label>
                                                        <br>
                                                        <img style="width:100px;" src="<?php echo base_url()?>assets/wahyu/images/logo/pdf.png" alt="Italian Trulli">
                                                        <br>
                                                        <a href=""><button type="button" class="btn btn-primary">buka</button></a>
                                                        <button type="button" class="btn btn-success">download</button>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div><!-- .portfolio-content -->
                                    </div><!-- .col -->
                                </div><!-- .col -->

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <div class="">
        <footer style="background-color: #284c7c;" class=" page-footer font-small blue">

            <!-- Copyright -->
            <div class="footer-copyright text-center  py-3">© 2018 Copyright:
                <a href="https://mdbootstrap.com/education/bootstrap/"> MDBootstrap.com</a>
            </div>
            <!-- Copyright -->
        </footer>
    </div>
    <script type='text/javascript' src='<?php base_url()?>assets/wahyu/js/jquery.js'></script>
    <script type='text/javascript' src='<?php base_url()?>assets/wahyu/js/custom.js'></script>

</body>

</html>