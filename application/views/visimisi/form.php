
<section class="content-header">
    <h1>
        Data Visi Misi
        <small>Form Input</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Visi Misi</a></li>
        <li class="active">Input</li>
    </ol>
</section>

<section class="content">

    <div class="box box-info">
        <div class="box-header">
            <i class="fa fa-table"></i>
            <h2 class="box-title">Input Visi Misi Prodi</h2>
           
        </div>
        <div class="box-body">
            <form action="<?php echo base_url(). 'Visimisi/insert'; ?>" method="post">
                <div class="form-group">

                    <div class="col-md-8">
                        <label>Jenis</label>
                        <select class="form-control" name="nama">
                            <option value="visi">Visi</option>
                            <option value="misi">Misi</option>
                        </select>
                    </div>


                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        <label>Isi</label>
                        <input type="text" class="form-control" name="isi" placeholder="isi"/>
                    </div>
                </div>


                <div class="form-group">
                    <div class="col-md-4">
                    <?php echo form_submit('SUBMIT','SIMPAN', array('class'=>'btn btn-primary btn-flat'));?>
                    <a href="<?php echo base_url()?>Visimisi" class="btn btn-danger btn-flat" role="button">KEMBALI</a>
                    </div>
                </div>


            </form>
        </div>

        <div class="box-footer clearfix">

        </div>
    </div>

</section>