
<section class="content-header">
    <h1>
        Visi Misi Program Studi
        <small>List</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Visi Misi</a></li>
        <li class="active">List</li>
    </ol>
</section>

<section class="content">

    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Daftar Visi Misi Prodi</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
        </div>

        <div class="box-header">


                <a href="<?php echo base_url(); ?>Visimisi/input" >
                    <button class="btn btn-success pull-right" type="button">
                        <div>
                            <i class="fa fa-plus-square"></i> TAMBAH
                        </div>
                    </button>
                </a>


        </div>

            <div class="box-body table-responsive no-padding">
                <table id="example1" class="Table_Data table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Jenis</th>
                        <th>Isi</th>

                        <th width="7%">Opsi</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($daftar as $value) {?>
                    <tr>
                        <td><input class="checkbox id_checkbox" type="checkbox" name="id[]" value="<?php echo $value->id; ?>" </td>
                        <td><?php echo $value->nama; ?></td>
                        <td><?php echo $value->isi; ?></td>


                        <td>
                            <?php
                            echo '<a href="'.base_url().'Visimisi/edit/'.$value->id.'" title="Edit Visi Misi">
                                    <button type="button" class="btn btn-xs btn-success btn-flat"><i class="fa fa-edit"></i></button></a>';
                            echo '<a href="'.base_url().'Visimisi/delete/'.$value->id.'" title="Delete Visi Misi">
                                    <button type="button" class="btn btn-xs btn-warning btn-flat"><i class="fa fa-trash-o"></i></button></a>';
                            ?>
                        </td>
                    </tr>
                    <?php } ?>

                    </tbody>
                </table>
            </div>
        </div>

</section>
 <script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>