
    <section class="content-header">
        <h1>
            Bahan Ajar
            <small>List</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Bahan Ajar</a></li>
            <li class="active">List</li>
        </ol>
    </section>

    <section class="content">


        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Daftar Bahan Ajar</h3>
                <div class="box-tools pull-right">
                </div>
            </div>
            <div class="box-header">


                <a href="<?php echo base_url(); ?>BahanAjar/input" >
                    <button class="btn btn-success pull-right" type="button">
                        <div>
                            <i class="fa fa-plus-square"></i> TAMBAH
                        </div>
                    </button>
                </a>


            </div>

            <!-- /.box-header -->
            <div class="box-body">
                <div style="overflow-x:auto;">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Nama Mata Kuliah</th>
                        <th>Unduh RPS (INA)</th>
                        <th>Unduh RPS (ENG)</th>
                        <th>Unduh BKPM (INA)</th>
                        <th>Unduh BKPM (ENG)</th>
                        <th>Unduh Silabus (INA)</th>
                        <th>Unduh Silabus (ENG)</th>

                        <th width="7%">Opsi</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($daftar as $value) {?>
                        <tr>
                            <td><input class="checkbox id_checkbox" type="checkbox" name="id[]" value="<?php echo $value->id; ?>" </td>
                            <td><?php echo $value->nama_matkul; ?></td>
                            <td> <a href="assets/uploads/<?php echo $value->rps_idn; ?>"><?php echo $value->rps_idn; ?> </a></td>
                            <td><a href="assets/uploads/<?php echo $value->rps_eng; ?>"><?php echo $value->rps_eng; ?> </a></td>
                            <td><a href="assets/uploads/<?php echo $value->bkpm_ind; ?>"><?php echo $value->bkpm_ind; ?> </a></td>
                            <td><a href="assets/uploads/<?php echo $value->bkpm_eng; ?>"><?php echo $value->bkpm_eng; ?> </a></td>
                            <td><a href="assets/uploads/<?php echo $value->silabus_idn; ?>"><?php echo $value->silabus_idn; ?> </a></td>
                            <td><a href="assets/uploads/<?php echo $value->silabus_eng; ?>"><?php echo $value->silabus_eng; ?> </a></td>

                            <td>
                                <?php
                                echo '<a href="'.base_url().'BahanAjar/edit/'.$value->id.'" title="Edit Bahan Ajar">
                                    <button type="button" class="btn btn-xs btn-success btn-flat"><i class="fa fa-edit"></i></button></a>';
                                echo '<a href="'.base_url().'BahanAjar/delete/'.$value->id.'" title="Delete Bahan Ajar">
                                    <button type="button" class="btn btn-xs btn-warning btn-flat"><i class="fa fa-trash-o"></i></button></a>';
                                ?>
                            </td>
                        </tr>
                    <?php } ?>

                    </tbody>
                </table>
            </div>
            </div>
         
        </div>

    </section>
     <script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>