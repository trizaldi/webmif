
<section class="content-header">
    <h1>
        Bahan Ajar
        <small>Form Input</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Bahan Ajar</a></li>
        <li class="active">Input</li>
    </ol>
</section>

<section class="content">

    <div class="box box-info">
        <div class="box-header">
            <i class="fa fa-table"></i>
            <h2 class="box-title">Input Bahan Ajar</h2>
           
        </div>
        <div class="box-body">
            <form action="<?php echo base_url(). 'BahanAjar/insert'; ?>" method="post" enctype="multipart/form-data">
                <div class="form-group row">
                    <div class="col-md-3">
                        <label>Mata Kuliah</label>
                         <select class="form-control" name="tm_matkul_id">
                           <?php foreach ($matkul as $row){?> 
                            <option value="<?php echo $row->id ?>"><?php echo $row->nama_matkul ?></option><?php } ?>
                         </select>
                    </div>
                    </div>

                <div class="form-group">
                <div class="form-group row">
                    <div class="col-md-6">
                        <div class="col-md12">
                            <label for="pic_file">Pilih File RPS (IND):</label>
                            <input type="file" name="userfile[]" class="form-control"  id="pic_file">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="col-md12">
                            <label for="pic_file">Pilih File RPS (ENG):</label>
                            <input type="file" name="userfile[]" class="form-control"  id="pic_file">
                        </div>
                    </div>
                </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        <div class="col-md12">
                            <label for="pic_file">Pilih File BKPM (IND):</label>
                            <input type="file" name="userfile[]" class="form-control"  id="pic_file">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="col-md12">
                            <label for="pic_file">Pilih File BKPM (ENG):</label>
                            <input type="file" name="userfile[]" class="form-control"  id="pic_file">
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-6">
                        <div class="col-md12">
                            <label for="pic_file">Pilih File Silabus (IND):</label>
                            <input type="file" name="userfile[]" class="form-control"  id="pic_file">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="col-md12">
                            <label for="pic_file">Pilih File Silabus (ENG):</label>
                            <input type="file" name="userfile[]" class="form-control"  id="pic_file">
                        </div>
                    </div>
                </div>



                <div class="form-group col-md-4 ">
                    <?php echo form_submit('SUBMIT','SIMPAN', array('class'=>'btn btn-primary btn-flat'));?>
                    <a href="<?php echo base_url()?>BahanAjar" class="btn btn-danger btn-flat" role="button">KEMBALI</a>
                    
                </div>


            </form>
        </div>
        <div class="box-footer clearfix">

        </div>
    </div>

</section>