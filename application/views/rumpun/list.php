
<section class="content-header">
    <h1>
        Rumpun Mata Kuliah
        <small>List</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Rumpun MK</a></li>
        <li class="active">List</li>
    </ol>
</section>

<section class="content">

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Daftar Rumpun Mata Kuliah</h3>
            <div class="box-tools pull-right">
            </div>
        </div>
        <div class="box-header">


            <a href="<?php echo base_url(); ?>Rumpun/input" >
                <button class="btn btn-success pull-right" type="button">
                    <div>
                        <i class="fa fa-plus-square"></i> TAMBAH
                    </div>
                </button>
            </a>


        </div>

            <div class="box-body table-responsive no-padding">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Id Rumpun</th>
                        <th>Rumpun Mata Kuliah</th>

                        <th width="7%">Opsi</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach (@$daftar as $value) {?>
                    <tr>
                        <td><input class="checkbox id_checkbox" type="checkbox" name="id[]" value="<?php echo $value->id; ?>" </td>
                        <td><?php echo $value->id; ?></td>
                        <td><?php echo $value->rumpun; ?></td>


                        <td>
                            <?php
                            echo '<a href="'.base_url().'Rumpun/edit/'.$value->id.'" title="Edit Rumpun Mata Kuliah">
                                    <button type="button" class="btn btn-xs btn-success btn-flat"><i class="fa fa-edit"></i></button></a>';
                            echo '<a href="'.base_url().'Rumpun/delete/'.$value->id.'" title="Delete Rumpun Mata Kuliah">
                                    <button type="button" class="btn btn-xs btn-warning btn-flat"><i class="fa fa-trash-o"></i></button></a>';
                            ?>
                        </td>
                    </tr>
                    <?php } ?>

                    </tbody>
                </table>
            </div>
       
        </div>

</section>

 <script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>
