
<section class="content-header">
    <h1>
        Data Rumpun Mata Kuliah
        <small>Form Input</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Rumpun</a></li>
        <li class="active">Input</li>
    </ol>
</section>

<section class="content">

    <div class="box">
        <div class="box-header">
            <i class="fa fa-table"></i>
            <h2 class="box-title">Input Rumpun Mata Kuliah</h2>
        </div>
        <div class="box-body">
            <form action="<?php echo base_url(). 'Rumpun/insert'; ?>" method="post">
                <div class="form-group">
                    <div class="col-md-3">
                        <label>Kode Rumpun Mata Kuliah</label>
                        <input type="text" class="form-control" name="id" placeholder="Kode Rumpun Mata Kuliah"/>
                    </div>
                    <div class="col-md-9">
                        <label>Nama Rumpun Mata Kuliah</label>
                        <input type="text" class="form-control" name="rumpun" placeholder="Rumpun Mata Kuliah"/>
                    </div>

                </div>
<div class="form-group">

</div>


                <div class="form-group">
                    <div class="col-md-4">
                    <?php echo form_submit('SUBMIT','SIMPAN', array('class'=>'btn btn-primary btn-flat'));?>
                    <a href="<?php echo base_url()?>Rumpun" class="btn btn-danger btn-flat" role="button">KEMBALI</a>
                    </div>
                </div>


            </form>
        </div>

        <div class="box-footer clearfix">

        </div>
    </div>

</section>