
<section class="content-header">
    <h1>
        Data Rumpun Mata Kuliah
        <small>Form Edit</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Rumpun</a></li>
        <li class="active">Edit</li>
    </ol>
</section>

<section class="content">

    <div class="box box-info">
        <div class="box-header">
            <i class="fa fa-table"></i>
            <h2 class="box-title">Edit Rumpun</h2>

        </div>
        <?php
        foreach ($rumpun as $value){
            //echo $value->id;


            ?>

        <div class="box-body">
            <form action="<?php echo base_url(). 'Rumpun/update'; ?>" method="post">
                <div class="form-group">
                    <div class="col-md-3">
                        <label>Id User</label>
                        <input type="text" class="form-control" name="id" readonly placeholder="id" value="<?php echo $value->id; ?>"/>
                    </div>
                    <div class="col-md-6">
                        <label>Username</label>
                        <input type="text" class="form-control" name="rumpun" placeholder="Username" value="<?php echo $value->rumpun; ?>"/>
                    </div>

                </div>


                <?php } ?>

                <div class="form-group">
                    <div class="col-md-4">
                    <?php echo form_submit('SUBMIT','SIMPAN', array('class'=>'btn btn-primary btn-flat'));?>
                    <a href="<?php echo base_url()?>Rumpun" class="btn btn-danger btn-flat" role="button">KEMBALI</a>
                    </div>
                </div>


            </form>
        </div>

        <div class="box-footer clearfix">

        </div>
    </div>

</section>