<?php
    $cek_user = $this->session->userdata("session_user");
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Manajemen Informatika</title>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/wahyu/css/bootstrap.min.css">

    <!-- FontAwesome CSS -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/wahyu/css/font-awesome.min.css">

    <!-- Styles -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/wahyu/style.css">
</head>

<body class="hero">
    <nav class="navbar navbar-expand-lg navbar-dark  fixed-top">
        <!--<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">-->
        <div class="container">
        <a class="navbar-brand"><img style="width: 300px;  " src="<?php echo base_url()?>assets/wahyu/images/mifmif.png"
                    height="50" width="100"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#"><b>Home</b>
                            <span class="sr-only">(current)</span>
                        </a>
                    </li>
                     <?php
              if($cek_user!=null){
                  echo '<li class="nav-item"><a class="nav-link" href="'.base_url('Auth/admin').'">Dashboard</a></li>';
                  echo '<li class="nav-item"><a class="nav-link">'.$cek_user.'</a></li>';
              }else{
                  echo '<li  class="nav-item"><a style="color: #0d273c" class="nav-link" href="'.base_url('auth').'"><b>Logi<b/>n</a></li>';
              }
              ?>
                    <!--<li class="nav-item">
                      <a class="nav-link" href="#">Services</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#">Contact</a>
                    </li>-->
                </ul>
            </div>
        </div>
    </nav>
    <div align=center class="outer-container">
        <div class="container portfolio-page">
            <div class="row">
                <div class="col">
                    <br>
                    <ul style="padding-top:80px;padding-bottom:10px;" class="breadcrumbs flex align-items-center">
                        <h1 style="color:#0d273c; padding-left:20px;" class="my-4 text-center text-lg-left">Menu Utama</h1>
                    </ul><!-- .breadcrumbs -->
                </div><!-- .col -->
            </div><!-- .row -->

            <div class="row">
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="portfolio-content">
                        <figure><a href="<?php echo base_url();?>Home/kurikulum" >
                            <img style="width:300px;height:150px;"  src="<?php echo base_url()?>assets/wahyu/images/logo/kurikulum.fw.png" alt=""></a>
                        </figure>
                    </div><!-- .portfolio-content -->
                </div><!-- .col -->

                <div class="col-12 col-md-6 col-lg-4">
                    <div class="portfolio-content">
                        <figure>
                        <a href="<?php echo base_url();?>Home/bahanajar">
                            <img style="width:300px;height:150px;" src="<?php echo base_url()?>assets/wahyu/images/logo/bahan_ajar.fw.png" alt="">
                            </a>
                        </figure>
                    </div><!-- .portfolio-content -->
                </div><!-- .col -->

                <div class="col-12 col-md-6 col-lg-4">
                    <div class="portfolio-content">
                        <figure>
                            <img style="width:300px;height:150px;" src="<?php echo base_url()?>assets/wahyu/images/logo/tugas_akhiri.fw.png" alt="">
                        </figure>
                    </div><!-- .portfolio-content -->
                </div><!-- .col -->


                <div class="col-12 col-md-6 col-lg-4">
                    <div class="portfolio-content">
                        <figure>
                            <img style="width:300px;height:150px;" src="<?php echo base_url()?>assets/wahyu/images/logo/pkl.fw.png" alt="">
                        </figure>
                    </div><!-- .portfolio-content -->
                </div><!-- .col -->

                <div class="col-12 col-md-6 col-lg-4">
                    <div class="portfolio-content">
                        <figure>
                            <img href="#" style="width:300px;height:150px;"  src="<?php echo base_url()?>assets/wahyu/images/logo/digilib.fw.png" alt="">
                        </figure>

                      
                    </div><!-- .portfolio-content -->
                </div><!-- .col -->

                <div class="col-12 col-md-6 col-lg-4">
                    <div class="portfolio-content">
                        <figure>
                            <img href="#" style="width:300px;height:150px;"  src="<?php echo base_url()?>assets/wahyu/images/logo/alumni.fw.png" alt="">
                        </figure>

                      
                    </div><!-- .portfolio-content -->
                </div><!-- .col -->

            </div><!-- .container -->
        </div><!-- .outer-container -->
    </div>
    </div>
    <div class=" fixed-bottom">
<?php $this->load->view("template/footer_home"); ?>
            </div>
    <script type='text/javascript' src='<?php echo base_url()?>assets/wahyu/js/jquery.js'></script>
    <script type='text/javascript' src='<?php echo base_url()?>assets/wahyu/js/custom.js'></script>

</body>

</html>