<?php
$cek_user = $this->session->userdata("session_user");
?>

<!DOCTYPE html>
<html lang="en"><head>
    <title>Manajemen Informatika</title>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/wahyu/css/bootstrap.min.css">

    <!-- FontAwesome CSS -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/wahyu/css/font-awesome.min.css">

    <!-- Styles -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/wahyu/style.css">
</head>


<body class="hero">
    <nav class="navbar navbar-expand-lg navbar-dark  fixed-top">
        <!--<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">-->
        <div class="container">
        <a class="navbar-brand"><img style="width: 32%;" src="<?php echo base_url()?>assets/wahyu/images/mifmif.png"
                    height="50" width="100"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                    <a class="nav-link" href="#"><b>Home</b>
                            <span class="sr-only">(current)</span>
                        </a>
                    </li>
                     <?php
              if($cek_user!=null){
                  echo '<li class="nav-item"><a class="nav-link" href="'.base_url('Auth/admin').'">Dashboard</a></li>';
                  echo '<li class="nav-item"><a class="nav-link">'.$cek_user.'</a></li>';
              }else{
                  echo '<li  class="nav-item"><a style="color: #0d273c" class="nav-link" href="'.base_url('auth').'"><b>Logi<b/>n</a></li>';
              }
                    ?>
                </li>
                </ul>
            </div>
        </div>
    </nav>



    <div class="outer-container">
        <div class="container portfolio-page">
            <div class="row">
                <div class="col">
                    <br>
                    <ul class="breadcrumbs flex align-items-center">
                    <ul style="padding-top:80px;padding-bottom:10px;" class="breadcrumbs flex align-items-center">
                    <p style="padding-top:15px;padding-left:1px; class="my-4 text-center text-lg-right"> <a href="http://localhost/webmif/Home/kurikulum" style="border-radius: 50%; background-color:#7d7978; widht:100px;" class="btn"><img src="<?php echo base_url()?>assets/wahyu/images/previous.png" alt=""></a>
<h1 style="padding-left:10px;color:#0d273c;" class="my-4 text-center text-lg-left">Document Sebaran Mata kuliah</h1>  </p>
                    </ul><!-- .breadcrumbs -->
                </div><!-- .col -->
            </div><!-- .row -->

            <!-- Footer -->

        </div><!-- .container -->
    </div><!-- .outer-container -->
    <div style="padding-bottom:100px;" class="sticky-top">
        <div class="container">
            <div style="border-radius: 5px;" class="padding-bottom:100px; card-group">
            <div  class="card ">
                    <div class="card-body text-center">
                       
                        <br>
                        <div  class="module">
                            <div class="module-body">
                                <div class="row">
                                 
                                    <div class="col-10 col-md-9 col-lg-12">
                                        <div class="portfolio-content">
                                             

                                                   <div class="box-body">
                <div style="overflow-x:auto;">
                <table class="table ">
                    <thead>
                    <tr>
                    <th><h2 style="color:#0d273c"><b>Document Sebaran Mata Kuliah</b></h2></th>
                     
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($data as $value) {?>
                        <tr>
                            <td>
<iframe height="600px" width="100%" src="<?php echo base_url()?>assets/uploads/sebaran_matkul/<?php echo $value->data_file; ?>" name="iframe_a"></iframe>

                            
                            </td>
                           
                        </tr>
                    <?php } ?>

                    </tbody>
                </table>
            </div>
            </div>
                                       
                                       
                                       
                                       
                                        </div><!-- .portfolio-content -->
                                    </div><!-- .col -->
                                </div><!-- .col -->

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <div class="">
<?php  $this->load->view("template/footer_home");?>

    </div>
    <script type='text/javascript' src='<?php base_url()?>assets/wahyu/js/jquery.js'></script>
    <script type='text/javascript' src='<?php base_url()?>assets/wahyu/js/custom.js'></script>
    
       <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
 
  
</body>

</html>