<?php
$cek_user = $this->session->userdata("session_user");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Manajemen Informatika</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/wahyu/css/bootstrap.min.css">
    <!-- FontAwesome CSS -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/wahyu/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/select/1.2.7/css/select.dataTables.min.css">
    <!-- Styles -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/wahyu/style.css">
    <!--metro4-->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/metro/style.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/table/css/style.css">
    <style>
        table, tbody {
  border: 2px solid black;
}
input{
       border:2px solid #FF0000;
    }
form {
  display: flex; /* 2. display flex to the rescue */
  flex-direction: row;
}
label, input ,select {
  display: block; /* 1. oh noes, my inputs are styled as block... */
}
</style>
</head>
<body class="hero">
    <nav class="navbar navbar-expand-lg navbar-dark  fixed-top">
        <!--<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">-->
        <div class="container">
            <a class="navbar-brand"><img style="width: 32%;" src="<?php echo base_url()?>assets/wahyu/images/mifmif.png"
                    height="50" width="100"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a style="color: #0d273c" class="nav-link" href="#"><b>Home</b>
                            <span class="sr-only">(current)</span>
                        </a>
                    </li>
                     <?php
              if($cek_user!=null){
                  echo '<li class="nav-item"><a class="nav-link" href="'.base_url('Auth/admin').'">Dashboard</a></li>';
                  echo '<li class="nav-item"><a class="nav-link">'.$cek_user.'</a></li>';
              }else{
                  echo '<li  class="nav-item"><a style="color: #0d273c" class="nav-link" href="'.base_url('auth').'"><b>Logi<b/>n</a></li>';
              }
              ?>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="outer-container">
        <div class="container portfolio-page">
            <div class="row">
                <div class="col">
                    <br>
                    <ul style="padding-top:80px;padding-bottom:10px;" class="breadcrumbs flex align-items-center">
                        <p style="padding-top:15px;padding-left:1px; class=" my-4 text-center text-lg-right"> <a href="http://localhost/webmif/"
                                style="border-radius: 50%; background-color:#7d7978; widht:100px;" class="btn"><img src="<?php echo base_url()?>assets/wahyu/images/previous.png"
                                    alt=""></a>
                            <h1 style="padding-left:10px; color:#0d273c;" class="my-4 text-center text-lg-left">Bahan
                                Ajar</h1>
                        </p>
                    </ul><!-- .breadcrumbs -->
                </div><!-- .col -->
            </div><!-- .row -->
            <!-- Footer -->
        </div><!-- .container -->
    </div><!-- .outer-container -->
    <div class="sticky-top">
        <div class="container">
            <div class=" card-group">
                <div style="background-color:white;" class="card ">
                    <div class="card-body text-center">
                        <br>
                        <div class="module">
                            <div class="module-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="portfolio-content">
                                            <div class="box-body">
                                                <div class="table-responsive">
                                                    <table class="table table-responsive">
                                                        <div class="container">
                                                            <div class="row">
                                                                <form>
                                                                    <select style=" padding-right:10px;width:20%; border:2px solid #0d273c;"
                                                                        class="form-control" id="exampleFormControlSelect1">
                                                                        <option>All</option>
                                                                        <option>Nama Mata kuliah</option>
                                                                        <option>3</option>
                                                                        <option>4</option>
                                                                        <option>5</option>
                                                                    </select>
                                                                    <input style="width:50%; border:2px solid #0d273c;"
                                                                        class="form-control" id="myInput" type="text"
                                                                        placeholder="Search..">
                                                            </div>
                                                            </form>
                                                            <br>
                                                        </div>
                                                </div>
                                                <thead class="thead">
                                                    <tr>
                                                        <th></th>
                                                        <th style="color:#ffffff;">No</th>
                                                        <th style="color:#ffffff;">Mata Kuliah</th>
                                                        <th style="color:#ffffff;">Unduh RPS (INA)</th>
                                                        <th style="color:#ffffff;">Unduh RPS (ENG)</th>
                                                        <th style="color:#ffffff;">Unduh BKPM (INA)</th>
                                                        <th style="color:#ffffff;">Unduh BKPM (ENG)</th>
                                                        <th style="color:#ffffff;">Unduh Silabus (INA)</th>
                                                        <th style="color:#ffffff;">Unduh Silabus (ENG)</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="myTable">
                                                    <?php
						                                $no=1;
					                                	foreach ($daftar as $value) {?>
                                                    <tr>
                                                        <td><input type="checkbox" name="vehicle1" value="Bike"></td>
                                                        <td>
                                                            <?php echo $no++ ?>
                                                        </td>
                                                        <td>
                                                            <?php echo $value->nama_matkul; ?>
                                                        </td>
                                                        <td><a href="<?php echo base_url()?>assets/uploads/bahan_ajar/<?php echo $value->rps_idn; ?>"
                                                                target="_blank">
                                                                <button class="btn btn-danger">Unduh</button>
                                                            </a></td>
                                                        <td><a href="<?php echo base_url()?>assets/uploads/bahan_ajar/<?php echo $value->rps_eng; ?>"
                                                                target="_blank"> <button class="btn btn-primary">Unduh</button>
                                                            </a></td>
                                                        <td><a href="<?php echo base_url()?>assets/uploads/bahan_ajar/<?php echo $value->bkpm_ind; ?>"
                                                                target="_blank"><button class="btn btn-danger">Unduh</button></a></td>
                                                        <td><a href="<?php echo base_url()?>assets/uploads/bahan_ajar/<?php echo $value->bkpm_eng; ?>"
                                                                target="_blank"><button class="btn btn-primary">Unduh</button></a></td>
                                                        <td><a href="<?php echo base_url()?>assets/uploads/bahan_ajar/<?php echo $value->silabus_idn; ?>"
                                                                target="_blank"><button class="btn btn-danger">Unduh</button></a></td>
                                                        <td><a href="<?php echo base_url()?>assets/uploads/bahan_ajar/<?php echo $value->silabus_eng; ?>"
                                                                target="_blank"><button class="btn btn-primary">Unduh</button>
                                                            </a></td>
                                                    </tr>
                                                    <?php } ?>
                                                </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- .portfolio-content -->
                            </div><!-- .col -->
                        </div><!-- .col -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <div class="fixed-bottom">
        <?php $this->load->view("template/footer_home"); ?>
    </div>
    <script type='text/javascript' src='<?php base_url()?>assets/wahyu/js/jquery.js'></script>
    <script type='text/javascript' src='<?php base_url()?>assets/wahyu/js/custom.js'></script>
    <script type='text/javascript' src='<?php base_url()?>assets/wahyu/js/demo.js'></script>
    <!--<script src="https://code.jquery.com/jquery-3.3.1.js"></script>  
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/select/1.2.7/js/dataTables.select.min.js"></script>
</body>
</html>
<script>
    $(document).ready(function () {
        $('#example1').DataTable({
            columnDefs: [{
                orderable: false,
                className: 'select-checkbox',
                targets: 0
            }],
            select: {
                style: 'os',
                selector: 'td:first-child'
            },
            order: [
                [1, 'asc']
            ]
        });
    });
</script>

<script>
    $(document).ready(function () {
        $("#myInput").on("keyup", function () {
            var value = $(this).val().toLowerCase();
            $("#myTable tr").filter(function () {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    });
</script>