<?php
$cek_user = $this->session->userdata("session_user");
?>

<!DOCTYPE html>
<html lang="en">

head>
    <title>Manajemen Informatika</title>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/wahyu/css/bootstrap.min.css">

    <!-- FontAwesome CSS -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/wahyu/css/font-awesome.min.css">

    <!-- Styles -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/wahyu/style.css">
</head>


<body class="hero">

<!-- Navigation -->
   <nav class="navbar navbar-expand-lg navbar-dark  fixed-top">
        <!--<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">-->
        <div class="container">
            <a class="navbar-brand"><img style="width: 20%;" src="http://jti.polije.ac.id/mif//assets/pic/logo_mif.png"
                    height="50" width="100"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="<?php echo base_url(); ?>">Home
                            <span class="sr-only">(current)</span>
                        </a>
                    </li>
                     <?php
              if($cek_user!=null){
                  echo '<li class="nav-item"><a class="nav-link" href="'.base_url('Auth/admin').'">Dashboard</a></li>';
                  echo '<li class="nav-item"><a class="nav-link">'.$cek_user.'</a></li>';
              }else{
                  echo '<li class="nav-item"><a class="nav-link" href="'.base_url('auth').'">Login</a></li>';
              }
              ?>
                    <!--<li class="nav-item">
                      <a class="nav-link" href="#">Services</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#">Contact</a>
                    </li>-->
                </ul>
            </div>
        </div>
    </nav>
     <div class="nav-bar-sep d-lg-none"></div>
<!-- Page Content -->
 <div class="outer-container">
        <div class="container single-portfolio">
            <div class="row">
                <div class="col-12">
                   <!-- <div class="featured-img">
                        <figure align=center>
                            <img style="height:500px;" src="images/logo/visi.png" alt="">
                        </figure>
                    </div> .content-area -->
                </div><!-- .col-12 -->
                <div style="padding-bottom:100px;" class="module">
                    <div class="module-body">
                        <div class="row">
                            <div style="background-color:rgb(190, 183, 183);" class="card ">
                                <div class="col-12 col-lg-9">
                                    <div style="padding: 60px;" class="content-area">
                                        <header class="entry-header">
                                           <h1 class="my-4 text-center text-lg-left">Visi Program Studi Manajemen Informatika <br><span class="small"> Jurusan Teknologi Informasi</span></h1>
                                        </header><!-- .entry-header -->
  
 <div class="entry-content">
 									<?php
	 									foreach ($daftar as $value){ ?>
                                             <p class="mb-0"> <?php echo $value->isi;}?>
                


                </p>
                                        </div><!-- .entry-content -->


                                    </div><!-- .content-area -->
                                </div><!-- .col-12 -->
                            </div>
                        </div>
                    </div>
                </div>

            </div><!-- .row -->

          
        </div><!-- .container -->
    </div><!-- .outer-container -->
            <div class="fixed-bottom">
<?php $this->load->view("template/footer_home"); ?>
	</div>
    </div>
    <script type='text/javascript' src='<?php echo base_url()?>assets/wahyu/js/jquery.js'></script>
    <script type='text/javascript' src='<?php echo base_url()?>assets/wahyu/js/custom.js'></script>

</body>

</html>