<?php
$cek_user = $this->session->userdata("session_user");
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>Manajemen Informatika</title>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/wahyu/css/bootstrap.min.css">

    <!-- FontAwesome CSS -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/wahyu/css/font-awesome.min.css">

    <!-- Styles -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/wahyu/style.css">
</head>


<body class="hero">

<!-- Navigation -->
   <nav class="navbar navbar-expand-lg navbar-dark  fixed-top">
        <!--<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">-->
       <div class="container">
       <a class="navbar-brand"><img style="width: 32%;" src="<?php echo base_url()?>assets/wahyu/images/mifmif.png"
                    height="50" width="100"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="<?php echo base_url()?>">Home
                            <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item">
                    <?php
                    if($cek_user!=null){
                        echo '<li class="nav-item"><a class="nav-link" href="'.base_url('Auth/admin').'">Dashboard</a></li>';
                        echo '<li class="nav-item"><a class="nav-link">'.$cek_user.'</a></li>';
                    }else{
                        echo '<li class="nav-item"><a class="nav-link" href="'.base_url('auth').'">Login</a></li>';
                    }
                    ?>
                </li>
                </ul>
            </div>
        </div>

    </nav>
     <div class="nav-bar-sep d-lg-none"></div>
<!-- Page Content -->
 <div class="outer-container">
 
        <div class="container single-portfolio">
     
            <div class="row">
              
              
            <ul style="padding:8px; " class="breadcrumbs flex align-items-center">
                    <p style="padding-top:15px;padding-left:1px; class="my-4 text-center text-lg-right"> <a href="http://localhost/webmif/Home/kurikulum" style="border-radius: 50%; background-color:#7d7978; widht:100px;" class="btn"><img src="<?php echo base_url()?>assets/wahyu/images/previous.png" alt=""></a>
<h1 style="padding-left:10px; color:#0d273c;" class="my-4 text-center text-lg-left">Misi Program Studi Manajemen Informatika <br><span class="small"> Jurusan Teknologi Informasi</span></h1>  </p>
                      
         
                    </ul><!-- .breadcrumbs -->
             
                    <!-- <div class="featured-img">
                        <figure align=center>
                            <img style="height:500px;" src="<?php echo base_url()?>images/logo/visi.png" alt="">
                        </figure>
                    </div>.content-area -->
              
                <div  class="module">
                    <div class="module-body">
                        <div class="row">
                            <div style="background-color:rgb(160, 183, 183);" class="card ">
                                <div class="col-12 col-lg-9">
                                    <div style="padding: 60px;" class="content-area">
                                        <header class="entry-header">
                                           <h1 class="my-4 text-center text-lg-left"></h1>
                                        </header><!-- .entry-header -->
  
 <div class="entry-content">
 									<?php
	 									foreach ($daftar as $value){ ?>
<tr>
                                           <table> <td style="width:5%;"><td><li> <b><?php echo $value->isi;}?><br></b></li></td>
                                           </tr>     </table>


                </p>
                                        </div><!-- .entry-content -->


                                    </div><!-- .content-area -->
                                </div><!-- .col-12 -->
                            </div>
                        </div>
                    </div> <div class="row">
                <div class="col">
                    <nav class="post-nav">
                        <ul class="flex justify-content-between align-items-center">
                            <li><a href="http://localhost/webmif/home/visi"><img src="<?php echo base_url()?>assets/wahyu/images/angle-left.png" alt="Previous"></a></li>
                            <li><a href=""><img src="" alt=""></a></li>
                            <li><a href="#"><img src="<?php echo base_url()?>assets/wahyu/images/angle-right.png" alt="Next"></a></li>
                        </ul>
                    </nav><!-- .post-nav -->
                </div><!-- .col -->
            </div><!-- .row -->
                </div>

            </div><!-- .row -->

          
        </div><!-- .container -->
    </div><!-- .outer-container -->
            <div class="fixed-bottom">
<?php $this->load->view("template/footer_home"); ?>
	</div>
    </div>
    <script type='text/javascript' src='<?php echo base_url()?>assets/wahyu/js/jquery.js'></script>
    <script type='text/javascript' src='<?php echo base_url()?>assets/wahyu/js/custom.js'></script>

</body>

</html>