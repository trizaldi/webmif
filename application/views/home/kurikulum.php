<?php
$cek_user = $this->session->userdata("session_user");
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>Manajemen Informatika - Kurikulum</title>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/wahyu/css/bootstrap.min.css">

    <!-- FontAwesome CSS -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/wahyu/css/font-awesome.min.css">

    <!-- Styles -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/wahyu/style.css">
</head>

<body class="hero">
    <nav class="navbar navbar-expand-lg navbar-dark  fixed-top">
        <!--<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">-->
        <div class="container">
        <a class="navbar-brand"><img style="width: 300px;  " src="<?php echo base_url()?>assets/wahyu/images/mifmif.png"
                    height="50" width="100"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button> 
            <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                    <a class="nav-link" href="#"><b>Home</b>
                            <span class="sr-only">(current)</span>
                        </a>
                    </li>
                     <?php
              if($cek_user!=null){
                  echo '<li class="nav-item"><a class="nav-link" href="'.base_url('Auth/admin').'">Dashboard</a></li>';
                  echo '<li class="nav-item"><a class="nav-link">'.$cek_user.'</a></li>';
              }else{
                  echo '<li  class="nav-item"><a style="color: #0d273c" class="nav-link" href="'.base_url('auth').'"><b>Logi<b/>n</a></li>';
              }
                    ?>
            </li>
                    <!--<li class="nav-item">
                      <a class="nav-link" href="#">Services</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#">Contact</a>
                    </li>-->
                </ul>
               
            </div>
        </div><!-- <img style="width: 10%;" src="http://jti.polije.ac.id/mif//assets/pic/logo_mif.png"
                    height="50" width="100">-->
    </nav>

  
    <div align=center class="outer-container">
    
        <div class="container portfolio-page">
            
    
            <div class="row">
           
                <div class="col">
                    <br>
                    
                    <ul style="padding-top:80px;padding-bottom:10px;" class="breadcrumbs flex align-items-center">
                     
<p style="padding-top:15px;padding-left:1px; class="my-4 text-center text-lg-right"> <a href="http://localhost/webmif/" style="border-radius: 50%; background-color:#7d7978; widht:100px;" class="btn"><img src="<?php echo base_url()?>assets/wahyu/images/previous.png" alt=""></a>
<h1 style="padding-left:10px; color:#0d273c;" class="my-4 text-center text-lg-left">Kurikulum</h1>  </p>

                    </ul><!-- .breadcrumbs -->
                    
                </div><!-- .col -->
            </div><!-- .row -->
           
            <div class="row">
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="portfolio-content">
                        <figure>
                        <a href="<?php echo base_url();?>home/visi">
                            <img style="width:300px;height:150px;"src="<?php echo base_url()?>assets/wahyu/images/logo/visi.PNG" alt="">
                            </a>
                        </figure>
                    </div><!-- .portfolio-content -->
                </div><!-- .col -->
                
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="portfolio-content">
                        <figure>
                        <a href="<?php echo base_url();?>home/misi">
                            <img style="width:300px;height:150px;" src="<?php echo base_url()?>assets/wahyu/images/logo/misi.PNG" alt="">
                            </a>
                        </figure>

                    </div><!-- .portfolio-content -->
                </div><!-- .col -->
                
                  <div class="col-12 col-md-6 col-lg-4">
                    <div class="portfolio-content">
                        <figure>
                        <a href="<?php echo base_url();?>home/capaian_matkul">
                            <img style="width:300px;height:150px;" src="<?php echo base_url()?>assets/wahyu/images/logo/pencapaian.PNG" alt="">
                            </a>
                        </figure>

                        
                    </div><!-- .portfolio-content -->
                </div><!-- .col -->
                

                <div class="col-12 col-md-6 col-lg-4">
                    <div class="portfolio-content">
                        <figure><a href="<?php echo base_url();?>home/matrik_matkul">
                            <img style="width:300px;height:150px;" src="<?php echo base_url()?>assets/wahyu/images/logo/matrik.PNG" alt="">
                        </a>
                        </figure>

                       
                    </div><!-- .portfolio-content -->
                </div><!-- .col -->


              

                <div class="col-12 col-md-6 col-lg-4">
                    <div class="portfolio-content">
                        <figure>
                        <a href="<?php echo base_url();?>home/sebaran_matkul">
                            <img style="width:300px;height:150px;" src="<?php echo base_url()?>assets/wahyu/images/logo/sebaran.PNG" alt="">
                            </a>
                        </figure>
                    </div><!-- .portfolio-content -->
                </div><!-- .col -->

                <div class="col-12 col-md-6 col-lg-4">
                    <div class="portfolio-content">
                        <figure>
                        <a href="<?php echo base_url();?>home/profil_matkul">
                            <img style="width:300px;height:150px;" src="<?php echo base_url()?>assets/wahyu/images/logo/profilmk.PNG" alt="">
                            </a>
                        </figure>

                    </div><!-- .portfolio-content -->
                </div><!-- .col -->

            </div><!-- .container -->
        </div><!-- .outer-container --><p style="padding-top:15px;padding-left:10px; class="my-4 text-center text-lg-right"> <a href="http://localhost/webmif/" style="border-radius: 10%; background-color:#7d7978; widht:100px;" class="btn"><img src="<?php echo base_url()?>assets/wahyu/images/home.png" alt=""></a>
  </p>
    </div>
    
    </div>
    <div class="fixed-bottom">
<?php $this->load->view("template/footer_home"); ?>
    </div>
    <script type='text/javascript' src='<?php echo base_url()?>assets/wahyu/js/jquery.js'></script>
    <script type='text/javascript' src='<?php echo base_url()?>assets/wahyu/js/custom.js'></script>

</body>

</html>