<div class="modal fade in" id="modal">
    <div class="modal-dialog moda-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">x</button>
                <div class="modal-title"><h5>Edit profile</h5></div>
            </div>
            <div class="modal-body">
                <form id="mb">
                    <input type="hidden" name="id" value="">

                    <div class="form-group">
                        <label>Nama lengkap</label>
                        <input type="text" name="nama" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Alamat</label>
                        <textarea class="form-control" name="alamat"></textarea>
                    </div>
                    <div class="form-group clearfix">
                        <button type="button" data-dismiss="modal">Batal</button>
                        <button type="button" id='btn' class="btn btn-success pull-right">Simapan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>