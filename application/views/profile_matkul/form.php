
<section class="content-header">
    <h1>
        Profile Mata Kuliah
        <small>Form Input</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Profile Mata Kuliah</a></li>
        <li class="active">Input</li>
    </ol>
</section>

<section class="content">

    <div class="box box-info">
        <div class="box-header">
            <i class="fa fa-table"></i>
            <h2 class="box-title">Input Profile Mata Kuliah</h2>
           
        </div>
        <div class="box-body">
            <form action="<?php echo base_url(). 'C_profile_matkul/insert'; ?>" method="post" enctype="multipart/form-data">

                <div class="form-group">
                <div class="form-group row">
                    <div class="col-md-6">
                        <div class="col-md12">
                            <label for="pic_file">Pilih File Profile Mata Kuliah :</label>
                            <input type="file" name="userfile[]" class="form-control"  id="pic_file">
                        </div>
                    </div>
                </div>
                </div>
                
                <div class="form-group col-md-4 ">
                    <?php echo form_submit('SUBMIT','SIMPAN', array('class'=>'btn btn-primary btn-flat'));?>
                    <a href="<?php echo base_url()?>C_profile_matkul" class="btn btn-danger btn-flat" role="button">KEMBALI</a>
                    
                </div>


            </form>
        </div>
        <div class="box-footer clearfix">

        </div>
    </div>

</section>