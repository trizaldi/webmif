
<section class="content-header">
    <h1>
        Mata Kuliah
        <small>List</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Mata Kuliah</a></li>
        <li class="active">List</li>
    </ol>
</section>

<section class="content">

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Daftar Mata Kuliah</h3>
            <div class="box-tools pull-right">
               
            </div>
        </div>
        <div class="box-header">

            <a href="<?php echo base_url(); ?>Matkul/input" >
                <button class="btn btn-success pull-right" type="button">
                    <div>
                        <i class="fa fa-plus-square"></i> TAMBAH
                    </div>
                </button>
            </a>


        </div>

            <div class="box-body">
                <table id="example" class="table table-striped" style="font-size:13px;">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Nama Mata Kuliah</th>
                        <th>Rumpun Mata Kuliah</th>
                        <th>Semester</th>
                        <th>SKS (Total)</th>
                        <th>SKS (Teori)</th>
                        <th>SKS (Praktek)</th>


                        <th width="7%">Opsi</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach (@$daftar as $value) {?>
                    <tr>
                        <td><input class="checkbox id_checkbox" type="checkbox" name="id[]" value="<?php echo $value->id; ?>" </td>
                        <td><?php echo $value->nama_matkul; ?></td>
                        <td><?php echo $value->rumpun; ?></td>
                        <td><?php echo $value->semester; ?></td>
                        <td><?php echo $value->sks; ?></td>
                        <td><?php echo $value->teori; ?></td>
                        <td><?php echo $value->praktek; ?></td>


                        <td>
                            <?php
                            echo '<a href="'.base_url().'Matkul/edit/'.$value->id.'" title="Edit Matkul">
                                    <button type="button" class="btn btn-xs btn-success btn-flat"><i class="fa fa-edit"></i></button></a>';
                            echo '<a href="'.base_url().'Matkul/delete/'.$value->id.'" title="Delete Matkul">
                                    <button type="button" class="btn btn-xs btn-warning btn-flat"><i class="fa fa-trash-o"></i></button></a>';
                            ?>
                        </td>
                    </tr>
                    <?php } ?>

                    </tbody>
                </table>
            </div>
        </div>

</section>


 <script>
 $(document).ready(function() {
    $('#example').DataTable();
} );
</script>
