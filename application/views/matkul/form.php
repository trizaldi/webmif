
<section class="content-header">
    <h1>
        Mata Kuliah
        <small>Form Input</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Mata Kuliah</a></li>
        <li class="active">Input</li>
    </ol>
</section>

<section class="content">

    <div class="box">
        <div class="box-header">
            <i class="fa fa-table"></i>
            <h2 class="box-title">Input Mata Kuliah</h2>
            <!-- tools box -->
            <!-- /. tools -->
        </div>
        <div class="box-body">
            <form action="<?php echo base_url(). 'Matkul/insert'; ?>" method="post">
                <div class="form-group">
                    <div class="col-md-3">
                        <label>Kode Mata Kuliah</label>
                        <input type="text" class="form-control" name="kode_matkul" placeholder="Kode Mata Kuliah"/>
                    </div>
                    <div class="col-md-9">
                        <label>Nama Mata Kuliah</label>
                        <input type="text" class="form-control" name="nama_matkul" placeholder="Judul Mata Kuliah"/>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6">
                    <label>Rumpun Mata Kuliah</label>
                         <select class="form-control" name="tm_matkul_id">
                           <?php foreach ($rumpun as $row){?> 
                            <option value="<?php echo $row->id ?>"><?php echo $row->rumpun ?></option><?php } ?>
                         </select>
                    </div>
                    <div class="col-md-6">
                    <label>Semester</label>
                    <input type="text" class="form-control" name="semester" placeholder="Semester"/>
                    </div>
                </div>
                <div class="col-md-12">
                <h4 > <i class="fa fa-scissors"></i> Pembagian SKS/Mata Kuliah</h4>
                </div>
                <hr>
                <div class="form-group col-md-4">
                    <label>SKS Mata Kuliah</label>
                    <input type="text" class="form-control" name="sks" placeholder="SKS Mata Kuliah"/>
                </div>
                <div class="form-group col-md-4">
                    <label>SKS Teori</label>
                    <input type="text" class="form-control" name="teori" placeholder="SKS Teori"/>
                </div>
                <div class="form-group col-md-4">
                    <label>SKS Praktek</label>
                    <input type="text" class="form-control" name="praktek" placeholder="SKS Praktek"/>
                </div>
                <div class="form-group col-md-4 ">
                    <?php echo form_submit('SUBMIT','SIMPAN', array('class'=>'btn btn-primary btn-flat'));?>
                    <a href="<?php echo base_url()?>Matkul" class="btn btn-danger btn-flat" role="button">KEMBALI</a>
                    
                </div>


            </form>
        </div>
        <div class="box-footer clearfix">

        </div>
    </div>

</section>