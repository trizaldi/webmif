<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">Menu Utama</li>
        <li class="active">
          <a href="<?php echo site_url('dashboard2') ?>">
            <i class="fa fa-home"></i> <span>Dashboard</span>
            <span class="pull-right-container">
              <small class="label pull-right"></small>
            </span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-newspaper-o"></i>
            <span>Data Master</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
           <li><a href="<?php echo site_url('User') ?>"><i class="fa fa-users"></i> Data User </a></li>
                    <li><a href="<?php echo site_url('Dosen') ?>"><i class="fa fa-clone"></i> Data Dosen </a></li>
                    <li><a href="<?php echo site_url('Matkul') ?>"><i class="fa fa-graduation-cap"></i> Data Mata Kuliah</a></li>
                    <li><a href="<?php echo site_url('Rumpun') ?>"><i class="fa fa-list"></i> Data Rumpun Mata Kuliah </a></li>
          </ul>
        </li>
      <li class="treeview">
          <a href="#">
            <i class="fa fa-camera"></i>
            <span>Kurikulum</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo site_url('Visimisi') ?>"><i class="fa fa-volume-up"></i> Data Visi Misi</a></li>
                    <li><a href="<?php echo site_url('C_capaian_matkul') ?>"><i class="fa fa-volume-up"></i> Data Capaian Pembelajaran</a></li>
                    <li><a href="<?php echo site_url('C_matrik_matkul') ?>"><i class="fa fa-volume-up"></i> Data Matrik Mata Kuliah</a></li>
                    <li><a href="<?php echo site_url('C_sebaran_matkul') ?>"><i class="fa fa-volume-up"></i> Data Sebaran Mata Kuliah</a></li>
                    <li><a href="<?php echo site_url('C_profile_matkul') ?>"><i class="fa fa-volume-up"></i> Data Profil mata kuliah</a></li>
        	</ul>
          </li>
       
        
        
         <li>
          <a href="<?php echo site_url('BahanAjar') ?>">
            <i class="fa fa-sign-out"></i> <span>Bahan Ajar</span>
            <span class="pull-right-container">
              <small class="label pull-right"></small>
            </span>
          </a>
        </li>
        
       
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>