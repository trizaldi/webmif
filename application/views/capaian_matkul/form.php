
<section class="content-header">
    <h1>
        Capaian Pembelajaran Lulusan
        <small>Form Input</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Capaian Pembelajaran Lulusan</a></li>
        <li class="active">Input</li>
    </ol>
</section>

<section class="content">

    <div class="box box-info">
        <div class="box-header">
            <i class="fa fa-table"></i>
            <h2 class="box-title">Input Capaian Pembelajaran Lulusan</h2>
           
        </div>
        <div class="box-body">
            <form action="<?php echo base_url(). 'C_capaian_matkul/insert'; ?>" method="post" enctype="multipart/form-data">

                <div class="form-group">
                <div class="form-group row">
                    <div class="col-md-6">
                        <div class="col-md12">
                            <label for="pic_file">Pilih File Capaian Pembelajaran Lulusan :</label>
                            <input type="file" name="userfile[]" class="form-control"  id="pic_file">
                        </div>
                    </div>
                </div>
                </div>
                
                <div class="form-group col-md-4 ">
                    <?php echo form_submit('SUBMIT','SIMPAN', array('class'=>'btn btn-primary btn-flat'));?>
                    <a href="<?php echo base_url()?>C_capaian_matkul" class="btn btn-danger btn-flat" role="button">KEMBALI</a>
                    
                </div>


            </form>
        </div>
        <div class="box-footer clearfix">

        </div>
    </div>

</section>