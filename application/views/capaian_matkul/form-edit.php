
<section class="content-header">
    <h1>
        Capaian Pembelajaran Lulusan
        <small>Form Edit</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Capaian Pembelajaran Lulusan</a></li>
        <li class="active">Edit</li>
    </ol>
</section>

<section class="content">

    <div class="box">
        <div class="box-header">
            <i class="fa fa-table"></i>
            <h2 class="box-title">Edit Capaian Pembelajaran Lulusan</h2>
        </div>

        <?php
        foreach ($capaian as $value){
			
            ?>

        <div class="box-body">
            <form action="<?php echo base_url(). 'C_capaian_matkul/update'; ?>" method="post" enctype="multipart/form-data">
               <div class="form-group row">
                    <div class="col-md-3">
                        <label>Kode Capaian Pembelajaran Lulusan</label>
                        <input type="text" class="form-control" name="id_capaian" placeholder="Kode Bahan Ajar" readonly value="<?php echo $value->id_capaian_matkul; ?>" />
                    </div>
                </div>
              
                <div class="form-group">
                <div class="form-group row">
                    <div class="col-md-6">
                        <div class="col-md12">
                            <label for="pic_file">Pilih File Capaian Pembelajaran Lulusan :</label>
                            <input type="file" name="userfile[]" class="form-control"  id="pic_file">
                        </div>
                    </div>
                    </div>

                <div class="form-group col-md-4 ">
                    <?php echo form_submit('SUBMIT','SIMPAN', array('class'=>'btn btn-primary btn-flat'));?>
                    <a href="<?php echo base_url()?>C_capaian_matkul" class="btn btn-danger btn-flat" role="button">KEMBALI</a>
                    
                </div>
                <?php } ?>

            </form>
        </div>
        <div class="box-footer clearfix">

        </div>
    </div>

</section>