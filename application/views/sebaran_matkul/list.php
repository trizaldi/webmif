
    <section class="content-header">
        <h1>
            Sebaran Mata Kuliah
            <small>List</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Sebaran Mata Kuliah</a></li>
            <li class="active">List</li>
        </ol>
    </section>

    <section class="content">


        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Daftar Sebaran Mata Kuliah</h3>
                <div class="box-tools pull-right">
                </div>
            </div>
            <div class="box-header">


                <a href="<?php echo base_url(); ?>C_sebaran_matkul/input" >
                    <button class="btn btn-success pull-right" type="button">
                        <div>
                            <i class="fa fa-plus-square"></i> TAMBAH
                        </div>
                    </button>
                </a>
            </div>

            <!-- /.box-header -->
            <div class="box-body">
                <div style="overflow-x:auto;">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Document Sebaran Mata Kuliah</th>
                        <th width="15%">Opsi</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($daftar as $value) {?>
                        <tr>
                            <td><input class="checkbox id_checkbox" type="checkbox" name="id[]" value="<?php echo $value->id_sebaran_matkul; ?>" </td>
                            <td><a href="assets/uploads/sebaran_matkul/<?php echo $value->data_file; ?>"><?php echo $value->data_file; ?> </a></td>

                            <td>
                                <?php
                                echo '<a href="'.base_url().'C_sebaran_matkul/edit/'.$value->id_sebaran_matkul.'" title="Edit Sebaran Matkul">
                                    <button type="button" class="btn btn-xs btn-success btn-flat"><i class="fa fa-edit"></i></button></a>';
                                echo '<a href="'.base_url().'C_sebaran_matkul/delete/'.$value->id_sebaran_matkul.'" title="Delete sebaran Matkul">
                                    <button type="button" class="btn btn-xs btn-warning btn-flat"><i class="fa fa-trash-o"></i></button></a>';
                                ?>
                            </td>
                        </tr>
                    <?php } ?>

                    </tbody>
                </table>
            </div>
            </div>
         
        </div>

    </section>
     <script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>