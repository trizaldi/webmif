
<section class="content-header">
    <h1>
        Sebaran Mata Kuliah
        <small>Form Edit</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Sebaran Mata Kuliah</a></li>
        <li class="active">Edit</li>
    </ol>
</section>

<section class="content">

    <div class="box">
        <div class="box-header">
            <i class="fa fa-table"></i>
            <h2 class="box-title">Edit Sebaran Mata Kuliah</h2>
        </div>

        <?php
        foreach ($sebaran as $value){
			
            ?>

        <div class="box-body">
            <form action="<?php echo base_url(). 'C_sebaran_matkul/update'; ?>" method="post" enctype="multipart/form-data">
               <div class="form-group row">
                    <div class="col-md-3">
                        <label>Kode Matrik Mata Kuliah</label>
                        <input type="text" class="form-control" name="id_sebaran" placeholder="Kode Sebaran Mata Kuliah" readonly value="<?php echo $value->id_sebaran_matkul; ?>" />
                    </div>
                </div>
              
                <div class="form-group">
                <div class="form-group row">
                    <div class="col-md-6">
                        <div class="col-md12">
                            <label for="pic_file">Pilih File Sebaran Mata Kuliah :</label>
                            <input type="file" name="userfile[]" class="form-control"  id="pic_file">
                        </div>
                    </div>
                    </div>

                <div class="form-group col-md-4 ">
                    <?php echo form_submit('SUBMIT','SIMPAN', array('class'=>'btn btn-primary btn-flat'));?>
                    <a href="<?php echo base_url()?>C_sebaran_matkul" class="btn btn-danger btn-flat" role="button">KEMBALI</a>
                    
                </div>
                <?php } ?>

            </form>
        </div>
        <div class="box-footer clearfix">

        </div>
    </div>

</section>