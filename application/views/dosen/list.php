<section class="content-header">
    <h1>
        Data Dosen
        <small>List</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Dosen</a></li>
        <li class="active">List</li>
    </ol>
</section>

<section class="content">

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Daftar Dosen</h3>
            <div class="box-tools pull-right">
            </div>
        </div>
        <div class="box-header">


            <a href="<?php echo base_url(); ?>Dosen/input" >
                <button class="btn btn-success pull-right" type="button">
                    <div>
                        <i class="fa fa-plus-square"></i> TAMBAH
                    </div>
                </button>
            </a>


        </div>


            <div class="box-body table-responsive no-padding">
                <table id="example" class="Table_Data table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Nama</th>
                        <th>NIP</th>
                        <th>NIDN</th>
                        <th>Jabatan</th>
                        <th>Prodi</th>
                        <th>Tempat Lahir</th>
                        <th>Tanggal Lahir</th>
                        <th>Alamat</th>
                        <th>Telepon</th>
                        <th>Email</th>
                        <th>Status Pegawai</th>
                        <th>Golongan</th>
                        <th>ID Sinta</th>
                        <th>Foto</th>
                        <th width="7%">Opsi</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach (@$daftar as $value) {?>
                    <tr>
                        <td><input class="checkbox id_checkbox" type="checkbox" name="id[]" value="<?php echo $value->id; ?>" </td>
                        <td><?php echo $value->nama; ?></td>
                        <td><?php echo $value->nip; ?></td>
                        <td><?php echo $value->nidn; ?></td>
                        <td><?php echo $value->jabatan; ?></td>
                        <td><?php echo $value->prodi; ?></td>
                        <td><?php echo $value->tmp_lahir; ?></td>
                        <td><?php echo $value->tgl_lahir; ?></td>
                        <td><?php echo $value->alamat; ?></td>
                        <td><?php echo $value->telp; ?></td>
                        <td><?php echo $value->email; ?></td>
                        <td><?php echo $value->pns; ?></td>
                        <td><?php echo $value->gol; ?></td>
                        <td><?php echo $value->id_sinta; ?></td>
                        <td><?php echo $value->image; ?></td>

                        <td>
                            <?php
                            echo '<a href="'.base_url().'Dosen/edit/'.$value->id.'" title="Edit Data Dosen">
                                    <button type="button" class="btn btn-xs btn-success btn-flat"><i class="fa fa-edit"></i></button></a>';
                            echo '<a href="'.base_url().'Dosen/delete/'.$value->id.'" title="Delete data Dosen">
                                    <button type="button" class="btn btn-xs btn-warning btn-flat"><i class="fa fa-trash-o"></i></button></a>';
                            ?>
                        </td>
                    </tr>
                    <?php } ?>

                    </tbody>
                </table>
            </div>

        </div>

</section>
<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>