<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Matkul extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('ModelMatkul');
		$this->load->model('ModelRumpun');
		$this->load->model('ModelLogin');

    }

	public function index()
	{
		$this->ModelLogin->getsqurity();
		$isi['daftar'] =$this->ModelMatkul->get_data();
		$isi['content'] ='matkul/list';
		//$isi['judul']	='Profile';
		//$isi['sub_judul']='Data Profile';
		$this->load->view('template/template',$isi);
	}

    public function input()
    {
		$this->ModelLogin->getsqurity();
       	$isi['content'] ='matkul/form';
		$isi['rumpun'] =$this->ModelRumpun->get_data();
		$this->load->view('template/template',$isi);
    }

    public function insert()
    {
        $matkul = array(
            'id' => $this->input->post('kode_matkul'),
            'nama_matkul' => $this->input->post('nama_matkul'),
            'tm_rumpun_id' => $this->input->post('rumpun'),
            'semester' => $this->input->post('semester'),
            'sks' => $this->input->post('sks'),
            'teori' => $this->input->post('teori'),
            'praktek' => $this->input->post('praktek'),

        );
        //var_dump($matkul);
        $this->db->insert('tm_matkul',$matkul);
        redirect('Matkul');
    }

    public function delete(){
        $id = $this->uri->segment(3);
        $this->db->where('id',$id);
        $data['msg']= null;
        if ($this->db->delete('tm_matkul')){
            $data['msg'] = "Hapus data berhasil !";
        }
        redirect('Matkul',$data);
    }

    public function edit(){
		$this->ModelLogin->getsqurity();
		$id=$this->uri->segment(3);
		$isi['bagi'] =$this->ModelMatkul->get_data_edit($id);
		$isi['rumpun'] =$this->ModelRumpun->get_data();
		$isi['content'] ='Matkul/form-edit';
		//$isi['judul']	='Profile';
		//$isi['sub_judul']='Data Profile';
		$this->load->view('template/template',$isi);
    }

    public function update(){
        $id=$this->uri->segment(3);
		$id_matkul=$this->input->post('kode_matkul');
        $data = array(
            'nama_matkul' => $this->input->post('nama_matkul'),
            'tm_rumpun_id' => $this->input->post('rumpun'),
            'semester' => $this->input->post('semester'),
            'sks' => $this->input->post('sks'),
            'teori' => $this->input->post('teori'),
            'praktek' => $this->input->post('praktek'),

        );
        //var_dump($data);
        $this->db->where('id',$id_matkul);
        $this->db->update('tm_matkul',$data);

        redirect('Matkul');
    }
}
