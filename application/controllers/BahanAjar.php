<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BahanAjar extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('ModelBahanAjar');
		$this->load->model('ModelMatkul');
        $this->load->helper('url');
        $this->load->helper('form');
		$this->load->model('ModelLogin');

    }
    private function set_upload_options(){
		
    $config = array();
    $config['upload_path'] = './assets/uploads/bahan_ajar';
    $config['allowed_types'] = 'gif|jpg|png|doc|docx|xlsx|xls|pdf|rar|zip';
    $config['max_size'] = '2048000';
    $config['overwrite']     = FALSE;

    return $config;
}

	public function index()
	{
		$this->ModelLogin->getsqurity();
		$isi['daftar'] =$this->ModelBahanAjar->get_data();
		$isi['content'] ='bahan_ajar/list';
		$this->load->view('template/template',$isi);
	}

    public function input()
    {
		$this->ModelLogin->getsqurity();
		$isi['content'] ='bahan_ajar/form';
		$isi['matkul']  =$this->ModelMatkul->get_data();
		$this->load->view('template/template',$isi);
    }

public function insert(){
	$this->load->library('upload');
    $dataInfo = array();
    $files = $_FILES;
    $cpt = count($_FILES['userfile']['name']);
    for($i=0; $i<$cpt; $i++)
    {           
        $_FILES['userfile']['name']= $files['userfile']['name'][$i];
        $_FILES['userfile']['type']= $files['userfile']['type'][$i];
        $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
        $_FILES['userfile']['error']= $files['userfile']['error'][$i];
        $_FILES['userfile']['size']= $files['userfile']['size'][$i];    

        $this->upload->initialize($this->set_upload_options());
        $this->upload->do_upload();
        $dataInfo[] = $this->upload->data();
    }

    $data = array(
        'tm_matkul_id' => $this->input->post('tm_matkul_id'),
        'rps_idn' => $dataInfo[0]['file_name'],
        'rps_eng' => $dataInfo[1]['file_name'],
        'bkpm_ind' => $dataInfo[2]['file_name'],
		'bkpm_eng' => $dataInfo[3]['file_name'],
        'silabus_idn' => $dataInfo[4]['file_name'],
        'silabus_eng' => $dataInfo[5]['file_name'],
     );
     	$this->db->insert('tm_bahan_ajar',$data);
	
redirect('BahanAjar');
	}
	
    public function delete(){
        $id = $this->uri->segment(3);
        $this->db->where('id',$id);
        $data['msg']= null;
        if ($this->db->delete('tm_bahan_ajar')){
            $data['msg'] = "Hapus data berhasil !";
        }
        redirect('BahanAjar',$data);
    }

    public function edit(){
		$this->ModelLogin->getsqurity();
        $id=$this->uri->segment(3);
		$isi['bahan'] =$this->ModelBahanAjar->get_data_edit($id);
		$isi['content'] ='bahan_ajar/form-edit';
		$isi['matkul']  =$this->ModelMatkul->get_data();
		$this->load->view('template/template',$isi);
    }

    public function update(){
		
    $this->load->library('upload');
    $dataInfo = array();
    $files = $_FILES;
    $cpt = count($_FILES['userfile']['name']);
    for($i=0; $i<$cpt; $i++)
    {           
        $_FILES['userfile']['name']		= $files['userfile']['name']	[$i];
        $_FILES['userfile']['type']		= $files['userfile']['type']	[$i];
        $_FILES['userfile']['tmp_name']	= $files['userfile']['tmp_name'][$i];
        $_FILES['userfile']['error']	= $files['userfile']['error']	[$i];
        $_FILES['userfile']['size']		= $files['userfile']['size']	[$i];    

        $this->upload->initialize($this->set_upload_options());
        $this->upload->do_upload();
        $dataInfo[] = $this->upload->data();
    }
     $data = array(
        'tm_matkul_id' => $this->input->post('tm_matkul_id'),
        'rps_idn' => $dataInfo[0]['file_name'],
        'rps_eng' => $dataInfo[1]['file_name'],
        'bkpm_ind' => $dataInfo[2]['file_name'],
		'bkpm_eng' => $dataInfo[3]['file_name'],
        'silabus_idn' => $dataInfo[4]['file_name'],
        'silabus_eng' => $dataInfo[5]['file_name'],
     );
	$id= $this->input->post("kode_bahan");
		

		$this->db->where('id',$id);
        $this->db->update('tm_bahan_ajar',$data);
	
redirect('BahanAjar');
		
	}
}
