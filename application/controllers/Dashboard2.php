<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard2 extends CI_Controller {
	    public function __construct()
    {
        parent::__construct();
        $this->load->model('ModelLogin');
    }

	public function index()
	{
		$this->ModelLogin->getsqurity();
		$isi['content'] ='dashboard2';
		$isi['judul']	='Profile';
		$isi['sub_judul']='Data Profile';
		$this->load->view('template/template',$isi);
	}
}
