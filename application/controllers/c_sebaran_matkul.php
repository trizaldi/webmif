<?php
class C_sebaran_matkul extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('ModelSebaran');
        $this->load->helper('form');
		$this->load->model('ModelLogin');
	}
	private function set_upload_options(){
		
    $config = array();
    $config['upload_path'] 		= './assets/uploads/sebaran_matkul';
    $config['allowed_types'] 	= 'gif|jpg|png|doc|docx|xlsx|xls|pdf|rar|zip';
    $config['max_size'] 		= '2048000';
    $config['overwrite']     	= FALSE;

    return $config;
}
	public function index(){
		$this->ModelLogin->getsqurity();
		$isi["daftar"]=$this->ModelSebaran->get_data();
		$isi['content']="sebaran_matkul/list";
		
		$this->load->view("template/template",$isi);
	}
	
	public function input()
    {
		$this->ModelLogin->getsqurity();
		$isi['content'] ='sebaran_matkul/form';
		
		$this->load->view('template/template',$isi);
    }
	
	public function insert(){
	$this->load->library('upload');
    $dataInfo = array();
    $files = $_FILES;
    $cpt = count($_FILES['userfile']['name']);
    for($i=0; $i<$cpt; $i++)
    {           
        $_FILES['userfile']['name']		= $files['userfile']['name']	[$i];
        $_FILES['userfile']['type']		= $files['userfile']['type']	[$i];
        $_FILES['userfile']['tmp_name']	= $files['userfile']['tmp_name'][$i];
        $_FILES['userfile']['error']	= $files['userfile']['error']	[$i];
        $_FILES['userfile']['size']		= $files['userfile']['size']	[$i];    

        $this->upload->initialize($this->set_upload_options());
        $this->upload->do_upload();
        $dataInfo[] = $this->upload->data();
    }
    $data = array(
		'data_file' 			=> $dataInfo[0]['file_name'],
     );
     	$this->db->insert('sebaran_matkul',$data);
	
redirect('c_sebaran_matkul');
	}
	
	public function delete(){
        $id = $this->uri->segment(3);
        $this->db->where('id_sebaran_matkul',$id);
        $data['msg']= null;
        if ($this->db->delete('sebaran_matkul')){
            $data['msg'] = "Hapus data berhasil !";
        }
        redirect('c_sebaran_matkul',$data);
    }

    public function edit(){
		$this->ModelLogin->getsqurity();
        $id=$this->uri->segment(3);
		$isi['sebaran'] =$this->ModelSebaran->get_data_edit($id);
		$isi['content'] ='sebaran_matkul/form-edit';
		$this->load->view('template/template',$isi);
    }

    public function update(){
		
    $this->load->library('upload');
    $dataInfo = array();
    $files = $_FILES;
    $cpt = count($_FILES['userfile']['name']);
    for($i=0; $i<$cpt; $i++)
    {           
        $_FILES['userfile']['name']		= $files['userfile']['name']	[$i];
        $_FILES['userfile']['type']		= $files['userfile']['type']	[$i];
        $_FILES['userfile']['tmp_name']	= $files['userfile']['tmp_name'][$i];
        $_FILES['userfile']['error']	= $files['userfile']['error']	[$i];
        $_FILES['userfile']['size']		= $files['userfile']['size']	[$i];    

        $this->upload->initialize($this->set_upload_options());
        $this->upload->do_upload();
        $dataInfo[] = $this->upload->data();
    }
    $data = array(
		'data_file' 			=> $dataInfo[0]['file_name'],
     );
	$id= $this->input->post("id_sebaran");
		

		$this->db->where('id_sebaran_matkul',$id);
        $this->db->update('sebaran_matkul',$data);
	
redirect('c_sebaran_matkul');
		
	}
		
}
?>