<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    function __construct(){
        parent::__construct();
        //$this->load->library(array('template','pagination','form_validation','upload'));
        $this->load->model('ModelUser');
		$this->load->model('ModelDosen');
		$this->load->model('ModelLogin');

    }

	public function index()
	{
		$this->ModelLogin->getsqurity();
		$isi['daftar'] =$this->ModelUser->get_data();
		$isi['content'] ='user/list';
		$isi['judul']	='Profile';
		$isi['sub_judul']='Data Profile';
		$this->load->view('template/template',$isi);
	}

    public function input()
    {
		$this->ModelLogin->getsqurity();
		$isi['content'] ='user/form';
		$isi['grup'] 		=$this->ModelUser->grup();
		$isi['dosen'] 		=$this->ModelDosen->get_data();
		$this->load->view('template/template',$isi);
	}

    public function insert()
    {
        $user = array(

            'username' => $this->input->post('username'),
            'password' => $this->input->post('password'),
            'tm_grup_id' => $this->input->post('tm_group_id'),
            'tm_dosen_id' => $this->input->post('tm_dosen_id'),


        );
        $this->db->insert('tm_users',$user);
        redirect('User');
    }

    public function delete(){
        $id=$this->uri->segment(3);
        $this->db->where_in('id',$id);
        $data['msg']= null;
        if ($this->db->delete('tm_users')){
            $data['msg'] = "Hapus data berhasil !";
        }
        redirect('User',$data);
    }

    public function edit(){
  
		$id					=$this->uri->segment(3);
		$isi['user'] 		=$this->ModelUser->get_data_edit($id);
		$isi['grup'] 		=$this->ModelUser->grup();
		$isi['dosen'] 		=$this->ModelDosen->get_data();
		$isi['content'] 	='user/form-edit';
		$isi['judul']		='Profile';
		$isi['sub_judul']	='Data Profile';
		$this->load->view('template/template',$isi);
    }

    public function update(){
        $id=$this->uri->segment(3);
		$id_user=$this->input->post('id');
        $user = array(
            'username' 		=> $this->input->post('username'),
            'password' 		=> $this->input->post('password'),
            'tm_grup_id' 	=> $this->input->post('tm_group_id'),
            'tm_dosen_id' 	=> $this->input->post('tm_dosen_id'),

        );
        $this->db->where('id',$id_user);
        $this->db->update('tm_users',$user);
        redirect('User');
    }
}
