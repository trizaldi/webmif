<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rumpun extends CI_Controller {

    function __construct(){
        parent::__construct();
        //$this->load->library(array('template','pagination','form_validation','upload'));
        $this->load->model('ModelRumpun');
		$this->load->model('ModelLogin');

    }

	public function index()
	{
		$this->ModelLogin->getsqurity();
		$isi['daftar'] =$this->ModelRumpun->get_data();
		$isi['content'] ='rumpun/list';
		$this->load->view('template/template',$isi);
	}

    public function input()
    {
		$this->ModelLogin->getsqurity();
		$isi['content'] ='rumpun/form';
		$this->load->view('template/template',$isi);
    }

    public function insert()
    {
        $user = array(
            'id' => $this->input->post('id'),
            'rumpun' => $this->input->post('rumpun'),



        );
        $this->db->insert('tm_rumpun',$user);
        redirect('Rumpun');
    }

    public function delete(){
        $id=$this->uri->segment(3);
        $this->db->where_in('id',$id);
        $data['msg']= null;
        if ($this->db->delete('tm_rumpun')){
            $data['msg'] = "Hapus data berhasil !";
        }
        redirect('Rumpun',$data);
    }

    public function edit(){
		$this->ModelLogin->getsqurity();
        $id=$this->uri->segment(3);
		$isi['rumpun'] =$this->ModelRumpun->get_data_edit($id);
		$isi['content'] ='rumpun/form-edit';
		$this->load->view('template/template',$isi);
    
    }

    public function update(){
        $id = $this->input->post('id');
        $user = array(
            'kduser' => $this->input->post('id_user'),
            'username' => $this->input->post('username'),
            'password' => $this->input->post('password'),
            'grupid' => $this->input->post('grupid'),
            'dosenid' => $this->input->post('dosenid'),

        );
        $this->db->where('id',$id);
        $this->db->update('tm_users',$user);
        redirect('User');
    }
}
