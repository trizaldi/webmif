<?php
class C_matrik_matkul extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('ModelMatrik');
		$this->load->model('ModelLogin');
        $this->load->helper('form');
	}
	private function set_upload_options(){
		
    $config = array();
    $config['upload_path'] 		= './assets/uploads/matrik_matkul';
    $config['allowed_types'] 	= 'gif|jpg|png|doc|docx|xlsx|xls|pdf|rar|zip';
    $config['max_size'] 		= '2048000';
    $config['overwrite']     	= FALSE;

    return $config;
}
	public function index(){
		$this->ModelLogin->getsqurity();
		$isi["daftar"]=$this->ModelMatrik->get_data();
		$isi['content']="matrik_matkul/list";
		
		$this->load->view("template/template",$isi);
	}
	
	public function input()
    {
		$this->ModelLogin->getsqurity();
		$isi['content'] ='matrik_matkul/form';
		
		$this->load->view('template/template',$isi);
    }
	
	public function insert(){
	$this->load->library('upload');
    $dataInfo = array();
    $files = $_FILES;
    $cpt = count($_FILES['userfile']['name']);
    for($i=0; $i<$cpt; $i++)
    {           
        $_FILES['userfile']['name']		= $files['userfile']['name']	[$i];
        $_FILES['userfile']['type']		= $files['userfile']['type']	[$i];
        $_FILES['userfile']['tmp_name']	= $files['userfile']['tmp_name'][$i];
        $_FILES['userfile']['error']	= $files['userfile']['error']	[$i];
        $_FILES['userfile']['size']		= $files['userfile']['size']	[$i];    

        $this->upload->initialize($this->set_upload_options());
        $this->upload->do_upload();
        $dataInfo[] = $this->upload->data();
    }
    $data = array(
		'data_file' 			=> $dataInfo[0]['file_name'],
     );
     	$this->db->insert('matrik_matkul',$data);
	
redirect('c_matrik_matkul');
	}
	
	public function delete(){
        $id = $this->uri->segment(3);
        $this->db->where('id_matrik_matkul',$id);
        $data['msg']= null;
        if ($this->db->delete('matrik_matkul')){
            $data['msg'] = "Hapus data berhasil !";
        }
        redirect('c_matrik_matkul',$data);
    }

    public function edit(){
		$this->ModelLogin->getsqurity();
        $id=$this->uri->segment(3);
		$isi['matrik'] =$this->ModelMatrik->get_data_edit($id);
		$isi['content'] ='matrik_matkul/form-edit';
		$this->load->view('template/template',$isi);
    }

    public function update(){
		
    $this->load->library('upload');
    $dataInfo = array();
    $files = $_FILES;
    $cpt = count($_FILES['userfile']['name']);
    for($i=0; $i<$cpt; $i++)
    {           
        $_FILES['userfile']['name']		= $files['userfile']['name']	[$i];
        $_FILES['userfile']['type']		= $files['userfile']['type']	[$i];
        $_FILES['userfile']['tmp_name']	= $files['userfile']['tmp_name'][$i];
        $_FILES['userfile']['error']	= $files['userfile']['error']	[$i];
        $_FILES['userfile']['size']		= $files['userfile']['size']	[$i];    

        $this->upload->initialize($this->set_upload_options());
        $this->upload->do_upload();
        $dataInfo[] = $this->upload->data();
    }
    $data = array(
		'data_file' 			=> $dataInfo[0]['file_name'],
     );
	$id= $this->input->post("id_matrik");
		

		$this->db->where('id_matrik_matkul',$id);
        $this->db->update('matrik_matkul',$data);
	
redirect('c_matrik_matkul');
		
	}
		
}
?>