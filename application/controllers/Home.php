<?php
class Home extends CI_Controller{
	function __construct(){
	    parent::__construct();
		$this->load->helper('url');
		$this->load->model('ModelKurikulum');
        $this->load->model('ModelVisi');
        $this->load->model('ModelMisi');
		$this->load->model('ModelBahanAjar');
		$this->load->model('ModelCapaian');
		$this->load->model('ModelMatrik');
		$this->load->model('ModelSebaran');
		$this->load->model('ModelProfile');
        $this->load->helper('form');
	}
	
	public function index(){
	    /*$ceklog = $this->session->userdata('session_user');
	    if($ceklog==null){
            $this->load->view('home/index');
        }else{
            $this->load->view('login');
        }*/
        $this->load->view('home/index');
	}

	public function kurikulum(){
	    $this->load->view('home/kurikulum');
    }

    public function bahanajar(){
		
		$isi['daftar']=$this->ModelBahanAjar->get_data();
	    $this->load->view('home/bahanajar',$isi);
    }
	public function profil_matkul(){
		
		$isi['data']=$this->ModelProfile->get_data();
	    $this->load->view('home/v_profile_matkul',$isi);
    }
	public function matrik_matkul(){
		
		$isi['data']=$this->ModelMatrik->get_data();
	    $this->load->view('home/v_matrik_matkul',$isi);
    }
	public function capaian_matkul(){
		
		$isi['data']=$this->ModelCapaian->get_data();
	    $this->load->view('home/v_capaian_matkul',$isi);
    }
	public function sebaran_matkul(){
		
		$isi['data']=$this->ModelSebaran->get_data();
	    $this->load->view('home/v_sebaran_matkul',$isi);
    }

    function visi() {
        //$this->load->view('home/visi');
        $visi = $this->ModelVisi->get_data();
        $data=array(
            "daftar"=>$visi
        );
        //var_dump($data);
        $this->load->view('home/visi', $data);
    }

    function misi() {
        //$this->load->view('home/visi');
        $misi = $this->ModelMisi->get_data();
        $data=array(
            "daftar"=>$misi
        );
        //var_dump($data);
        $this->load->view('home/misi', $data);
    }
}
?>