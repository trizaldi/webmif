<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Visimisi extends CI_Controller {

    function __construct(){
        parent::__construct();
		$this->load->model('ModelLogin');
        $this->load->model('ModelVisimisi');

    }

	public function index()
	{
		$this->ModelLogin->getsqurity();
		$isi['daftar'] =$this->ModelVisimisi->get_data();
		$isi['content'] ='visimisi/list';
		//$isi['judul']	='Profile';
		//$isi['sub_judul']='Data Profile';
		$this->load->view('template/template',$isi);
	}

    public function input()
    {
		$this->ModelLogin->getsqurity();
       $isi['content'] ='visimisi/form';
		//$isi['judul']	='Profile';
		//$isi['sub_judul']='Data Profile';
		$this->load->view('template/template',$isi);
    }

    public function insert()
    {
        $user = array(

            'nama' => $this->input->post('nama'),
            'isi' => $this->input->post('isi'),



        );
        $this->db->insert('tm_visimisi',$user);
        redirect('Visimisi');
    }

    public function delete(){
        $id=$this->uri->segment(3);
        $this->db->where_in('id',$id);
        $data['msg']= null;
        if ($this->db->delete('tm_visimisi')){
            $data['msg'] = "Hapus data berhasil !";
        }
        redirect('Visimisi',$data);
    }

    public function edit(){
		$this->ModelLogin->getsqurity();
        $id=$this->uri->segment(3);
		$isi['visimisi']	=$this->ModelVisimisi->get_data_edit($id);
		$isi['content'] 	='visimisi/form-edit';
		$this->load->view('template/template',$isi);
    }

    public function update(){
        $id = $this->input->post('id');
        $data = array(
            'nama' => $this->input->post('nama'),
            'isi' => $this->input->post('isi'),
            
        );
        $this->db->where('id',$id);
        $this->db->update('tm_visimisi',$data);
        redirect('Visimisi');
    }
}
