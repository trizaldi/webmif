<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dosen extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('ModelDosen');
		$this->load->model('ModelLogin');

    }

	public function index()
	{
		$this->ModelLogin->getsqurity();
		$isi['daftar'] =$this->ModelDosen->get_data();
		$isi['content'] ='dosen/list';
		$this->load->view('template/template',$isi);
	}

    public function input()
    {
		$this->ModelLogin->getsqurity();
		$isi['content'] ='dosen/form';
		$this->load->view('template/template',$isi);
    }

    public function insert()
    {
        $user = array(
            'kduser' => $this->input->post('id_user'),
            'username' => $this->input->post('username'),
            'password' => $this->input->post('password'),
            'grupid' => $this->input->post('grupid'),
            'dosenid' => $this->input->post('dosenid'),


        );
        $this->db->insert('tm_users',$user);
        redirect('User');
    }

    public function delete(){
        $id = $this->input->post('id');
        $this->db->where_in('id',$id);
        $data['msg']= null;
        if ($this->db->delete('tm_users')){
            $data['msg'] = "Hapus data berhasil !";
        }
        redirect('User',$data);
    }

    public function edit(){
		$this->ModelLogin->getsqurity();
        $id=$this->uri->segment(3);
		$isi['user'] 		=$this->ModelUser->get_data_edit($id);
		$isi['content'] 	='User/form';
		$this->load->view('template/template',$isi);
        //$user = $this->ModelUser->get_data_edit($id);
        //$data = array(
          //  'daftar'=>$user
        //);
        //var_dump($data);
        //$this->load->view('User/form');
    }

    public function update(){
        $id = $this->input->post('id');
        $user = array(
            'kduser' => $this->input->post('id_user'),
            'username' => $this->input->post('username'),
            'password' => $this->input->post('password'),
            'grupid' => $this->input->post('grupid'),
            'dosenid' => $this->input->post('dosenid'),

        );
        $this->db->where('id',$id);
        $this->db->update('tm_users',$user);
        redirect('User');
    }
}
