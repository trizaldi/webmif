<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 5/28/2018
 * Time: 3:52 PM
 */

class ModelBahanAjar extends CI_Model{

    public function get_data(){
        $query = $this->db->query("SELECT tm_bahan_ajar.id, nama_matkul, rps_idn, rps_eng, bkpm_ind, bkpm_eng, silabus_idn, silabus_eng FROM `tm_bahan_ajar` JOIN tm_matkul ON tm_bahan_ajar.tm_matkul_id=tm_matkul.id");
        return $query->result();

    }

    public function get_data_edit($id){
        $query = $this->db->query("SELECT tm_bahan_ajar.id, tm_matkul_id, nama_matkul, rps_idn, rps_eng, bkpm_ind, bkpm_eng, silabus_idn, silabus_eng FROM `tm_bahan_ajar` JOIN tm_matkul ON tm_bahan_ajar.tm_matkul_id=tm_matkul.id where tm_bahan_ajar.id='$id'");
        return $query->result();

    }
}