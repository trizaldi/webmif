<?php
	class ModelLogin extends CI_Model {
		function __construct(){
			parent::__construct();
			$this->load->database();
		}

		public function login($username, $password){
			$query = $this->db->get_where('tm_users', array('username'=>$username, 'password'=>$password));
			return $query->row_array();
		}
		public function getsqurity()
	{
		$username=$this->session->userdata('username');
		if(empty($username))
		{
			$this->session->sess_destroy();
			redirect ('Auth');
		}
	}

	}
?>