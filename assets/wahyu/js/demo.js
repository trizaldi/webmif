require(['ojs/ojcore', 'knockout', 'jquery', 'ojs/ojknockout', 'ojs/ojtable', 'ojs/ojcheckboxset', 'ojs/ojarraytabledatasource', 'ojs/ojlabel', 'ojs/ojbutton'],
	function (oj, ko, $) {
		function viewModel() {
			var self = this;
			self.deptArray = [{
					DepartmentId: 1001,
					DepartmentName: 'ADFPM 1001 neverending',
					LocationId: 200,
					ManagerId: 300
				},
				{
					DepartmentId: 556,
					DepartmentName: 'BB',
					LocationId: 200,
					ManagerId: 300
				},
				{
					DepartmentId: 10,
					DepartmentName: 'Administration',
					LocationId: 200,
					ManagerId: 300
				},
				{
					DepartmentId: 20,
					DepartmentName: 'Marketing',
					LocationId: 200,
					ManagerId: 300
				},
				{
					DepartmentId: 30,
					DepartmentName: 'Purchasing',
					LocationId: 200,
					ManagerId: 300
				},
				{
					DepartmentId: 40,
					DepartmentName: 'Human Resources1',
					LocationId: 200,
					ManagerId: 300
				},
				{
					DepartmentId: 50,
					DepartmentName: 'Administration2',
					LocationId: 200,
					ManagerId: 300
				},
				{
					DepartmentId: 60,
					DepartmentName: 'Marketing3',
					LocationId: 200,
					ManagerId: 300
				},
				{
					DepartmentId: 70,
					DepartmentName: 'Purchasing4',
					LocationId: 200,
					ManagerId: 300
				},
				{
					DepartmentId: 80,
					DepartmentName: 'Human Resources5',
					LocationId: 200,
					ManagerId: 300
				},
				{
					DepartmentId: 90,
					DepartmentName: 'Human Resources11',
					LocationId: 200,
					ManagerId: 300
				},
				{
					DepartmentId: 100,
					DepartmentName: 'Administration12',
					LocationId: 200,
					ManagerId: 300
				},
				{
					DepartmentId: 110,
					DepartmentName: 'Marketing13',
					LocationId: 200,
					ManagerId: 300
				},
				{
					DepartmentId: 120,
					DepartmentName: 'Purchasing14',
					LocationId: 200,
					ManagerId: 300
				},
				{
					DepartmentId: 130,
					DepartmentName: 'Human Resources15',
					LocationId: 200,
					ManagerId: 300
				}
			];
			self.datasource = new oj.ArrayTableDataSource(self.deptArray, {
				idAttribute: 'DepartmentId'
			});
			self.columnArray = [{
					"headerText": "Selected",
					"template": "checkTemplate"
				},
				{
					"headerText": "Department Id",
					"field": "DepartmentId"
				},
				{
					"headerText": "Department Name",
					"field": "DepartmentName"
				},
				{
					"headerText": "Location Id",
					"field": "LocationId"
				},
				{
					"headerText": "Manager Id",
					"field": "ManagerId"
				}
			];

			self.selectedItems = ko.observable([]);

			// get index of range containing id
			self.getIndexInSelectedItems = function (id) {
				for (var i = 0; i < self.selectedItems().length; i++) {
					var range = self.selectedItems()[i];
					var startIndex = range.startIndex.row;
					var endIndex = range.endIndex.row;
					if (id >= startIndex && id <= endIndex) {
						return i;
					}
				}
				return -1;
			};

			// get checkbox selected value based on selectedItems and selectAll state
			self.handleCheckbox = function (id) {
				var isChecked = self.getIndexInSelectedItems(id) != -1;
				return isChecked ? ["checked"] : [];
			};

			self.checkboxListener = function (event) {
				if (event.detail != null) {
					var value = event.detail.value;
					var newSelectedItems;
					var id = event.target.dataset.rowId;

					// need to convert to Number to match the DepartmentId key type
					var key = Number(event.target.dataset.rowKey);
					var totalSize = self.datasource.totalSize();
					if (value.length > 0) {
						newSelectedItems = self.getIndexInSelectedItems(id) == -1 ?
							self.selectedItems().concat([{
								startIndex: {
									row: id
								},
								endIndex: {
									row: id
								},
								startKey: {
									row: key
								},
								endKey: {
									row: key
								}
							}]) :
							self.selectedItems();
					} else {
						newSelectedItems = self.selectedItems();
						var idx = self.getIndexInSelectedItems(id);
						if (idx != -1) {
							var item = newSelectedItems.splice(idx, 1)[0];

							// break up ranges into two separate ranges if index is in range;
							if (item.startIndex.row != item.endIndex.row) {
								var newItems = [];
								if (id != 0) {
									newItems.push({
										startIndex: {
											row: item.startIndex.row
										},
										endIndex: {
											row: Number(id) - 1
										}
									})
								}
								if (id != self.datasource.totalSize() - 1) {
									newItems.push({
										startIndex: {
											row: Number(id) + 1
										},
										endIndex: {
											row: item.endIndex.row
										}
									})
								}
								newSelectedItems = newSelectedItems.concat(newItems);
							}
						}
					}
					self.selectedItems(newSelectedItems);
				}
			};

			self.selectionListener = function (event) {
				// show current selection in textarea
				self.printCurrentSelection();
			};

			self.printCurrentSelection = function () {
				var i = 0;
				var selectionTxt = "";
				for (i = 0; i < self.selectedItems().length; i++) {
					var range = self.selectedItems()[i];
					var startIndex = range.startIndex;
					var endIndex = range.endIndex;
					var startKey = range.startKey;
					var endKey = range.endKey;

					if (startIndex != null && startIndex.row != null) {
						//row selection
						selectionTxt = selectionTxt + "Row Selection\n";
						selectionTxt = selectionTxt + "start row index: " + startIndex.row + ", end row index: " + endIndex.row + "\n";
					}
					if (startKey != null && startKey.row != null) {
						selectionTxt = selectionTxt + "start row key: " + startKey.row + ", end row key: " + endKey.row + "\n";
					}
				}
				document.getElementById("selectionCurrent").value = selectionTxt;
			};
		}
		var vm = new viewModel;

		$(document).ready(
			function () {
				var table = document.getElementById('table');
				ko.applyBindings(vm, table);

				// Need to block event propagation on checkbox click 
				// to prevent default table row selection 
				table.addEventListener("click", function (event) {
					if (event.target.className.indexOf("oj-checkbox") != -1) {
						event.stopPropagation();
					}
				}, true)
			}
		);
	});
